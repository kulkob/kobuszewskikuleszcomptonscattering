/**
 * The package contains classes crucial for computations.
 */
/**
 * @author Konrad Kobuszewski
 *
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.computations;