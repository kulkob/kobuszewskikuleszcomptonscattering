package pl.pw.edu.fizyka.pojava.comptoncrosssections.computations;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.List;

import javax.measure.quantity.Energy;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.jscience.physics.amount.Amount;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * 
 * @author Konrad Kobuszewski
 *
 */
public class Engine extends SwingWorker<Void, Integer> {
	
	int counter = 20000;
	boolean loopInProgress = true;
	JProgressBar progressBar = null;
	JDialog dialog = null;
	DBConnector db = null;
	MainWindow frame;
	
	public Engine(MainWindow frame) {
		// TODO Auto-generated constructor stub
		this.frame = frame;
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setVisible(true);
		dialog = new JDialog();
		dialog.add(progressBar,BorderLayout.SOUTH);
		dialog.setLocationRelativeTo(null);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setAlwaysOnTop(false);
		dialog.add(	new JLabel(frame.messages.getString("Engine.progressDialog")),
					BorderLayout.CENTER);
		dialog.setSize(new Dimension(200,100));
				
		//PropertyChangeListener for changing JProgressBar
		addPropertyChangeListener(new PropertyChangeListener() {
	        @Override
	        public void propertyChange(PropertyChangeEvent evt) {
	            progressBar.setValue(getProgress());
	        }
	    });
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		// TODO Auto-generated method stub

		try {
			db = new DBConnector();
			db.createTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dialog.setVisible(true);
		do	{
			counter--;
						
			RandomEnergy randomEnergy = new RandomEnergy();
			RandomAngle randomAngle = new RandomAngle();
						
			Amount<Energy> inEnergyKeV = randomEnergy.rand();					
			db.insertData(counter, inEnergyKeV, randomAngle.randAngle(inEnergyKeV));
			
			publish(counter);
		}	while( counter != 0 && loopInProgress);
		
		return null;
	}
	
	@Override
	protected void done(){
		dialog.dispose();
		frame.engine = new Engine(frame);
		frame.btnGenerateData.setEnabled(true);
		frame.btnGenerateData.setSelected(false);
	}
	
	@Override
	protected void process(List<Integer> progress){
		for(int prog : progress){
			setProgress(100-prog/200);
		}
	}
	
	public void stop(){
		loopInProgress = false;
	}
}