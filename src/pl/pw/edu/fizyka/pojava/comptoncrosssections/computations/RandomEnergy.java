package pl.pw.edu.fizyka.pojava.comptoncrosssections.computations;


import java.security.SecureRandom;
import java.util.ArrayList;
import javax.measure.quantity.Energy;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jscience.physics.amount.Amount;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;


/**
 * This class implements methods for drawing random energy from a discrete
 * distribution of energy (containing value of energetic channel and its intensity)
 *  
 * @author Konrad Kobuszewski
 */
public class RandomEnergy {
	
	int endOfInterval=0;
	double maxIntensity=0;
	SecureRandom uniformDistribution;
	ArrayList<ImmutablePair<Amount<Energy>,Double>> energyDist;
	
	
	/**
	 * @deprecated
	 * Constructor of class RandomEnergy. Sets fields endOfInterval and maxIntensity
	 * @param incidentenergyDist incident energy distribution, variable of type
	 * ArrayList<ImmutablePair<Amount<Energy>,Double>>
	 */
	public RandomEnergy(
			ArrayList<ImmutablePair<Amount<Energy>,Double>> incidentenergyDist) {
		// TODO Auto-generated constructor stub
		energyDist = new ArrayList<ImmutablePair<Amount<Energy>,Double>>();
		energyDist = incidentenergyDist;
		endOfInterval = energyDist.size();
		
		//Sets maximal intensity of stream of photons
		for(int ii = 0; ii<endOfInterval; ii++)
		{
			ImmutablePair<Amount<Energy>, Double> currentEnergy = energyDist.get(ii);
			if(currentEnergy.getRight()>maxIntensity)
				maxIntensity=currentEnergy.getRight();
		}
		
		uniformDistribution = new SecureRandom();
	}
	
	/**
	 * Constructor of class RandomEnergy. Sets fields endOfInterval and maxIntensity
	 */
	public RandomEnergy() {
		// TODO Auto-generated constructor stub
		energyDist = Main.storage.getEnergyDistrib();
				
		endOfInterval = energyDist.size();		
		for(int ii = 0; ii<endOfInterval; ii++)
		{
			ImmutablePair<Amount<Energy>, Double> currentEnergy = energyDist.get(ii);
			if(currentEnergy.getRight()>maxIntensity)
				maxIntensity=currentEnergy.getRight();
		}
		
		uniformDistribution = new SecureRandom();
	}
	
	/**
	 * Draws random energy value from distribution, where the probability of value 
	 * is given by intensity of energetic channel.
	 * 
	 * @return value of energetic channel in the same unit as distribution energy.
	 */
	public Amount<Energy> rand(){
		Amount<Energy> drawedEnergyValue=null;
		
		if(energyDist.size() == 1)
			return energyDist.get(0).getLeft();
		else{
			//algorytm von Neumanna
			while(drawedEnergyValue==null){
				int energeticChannel =uniformDistribution.nextInt(endOfInterval);
				ImmutablePair<Amount<Energy>,Double> currentEnergy =
						energyDist.get(energeticChannel);
				
				if(uniformDistribution.nextDouble() < (currentEnergy.getRight()/maxIntensity) )
					drawedEnergyValue=currentEnergy.getLeft();		
			}
			return drawedEnergyValue;
		}
	}
	
}
