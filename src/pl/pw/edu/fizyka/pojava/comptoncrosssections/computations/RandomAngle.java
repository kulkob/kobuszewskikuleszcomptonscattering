package pl.pw.edu.fizyka.pojava.comptoncrosssections.computations;

import java.security.SecureRandom;
import java.util.Random;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

import org.jscience.physics.amount.Amount;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.Results;



/**
 * This class provides methods to drawing angles from distribution
 * of probability given by Klein-Nishina formula.
 * 
 * @author Konrad Kobuszewski
 */
public class RandomAngle {
	
	double incidentPhotonEnergy=0;
	double scatteredPhotonEnergy=0;
	double beginingOfInterval=0;
	double endOfInterval=180;
	double maxIntensity=0;
		
	Random uniformDistribution;
	
	/**
	 * Constructor of class RandomAngle
	 */
	public RandomAngle() {
		// TODO Auto-generated constructor stub
		uniformDistribution = new SecureRandom();
	}
	
	/**
	 * @deprecated
	 * Draws random angle, where the density of probability 
	 * 
	 * @return angle in degrees from Klein-Nishina distribution.
	 */
	public double rand(double energyValue){
		double angleOfScatteringDeg=0;
		
		maxIntensity=kleinNishinaFormula(energyValue, 0);
		System.out.println("maxIntensity:"+maxIntensity);
		//algorytm von Neumanna
		while(angleOfScatteringDeg==0){
			double angleDeg =(endOfInterval-beginingOfInterval)*uniformDistribution.nextDouble();
			
			if(uniformDistribution.nextDouble()*maxIntensity <
					kleinNishinaFormula(energyValue, angleDeg))
				angleOfScatteringDeg=angleDeg;
		}
		
		if(uniformDistribution.nextBoolean()==true)
			angleOfScatteringDeg=360-angleOfScatteringDeg;
		
		return angleOfScatteringDeg;
	}
	
	/**
	 * 
	 * Draws random angle, where the density of probability 
	 * 
	 * @return angle in degrees from Klein-Nishina distribution.
	 */
	public Amount<Angle> randAngle(Amount<Energy> currentEnergy){
		Amount<Angle> angleOfScatteringDeg = null;
		
		maxIntensity=kleinNishinaFormula(
				Amount.valueOf(0,SI.RADIAN), currentEnergy);
		
		//algorytm von Neumanna
		while(angleOfScatteringDeg==null){
			double angleDeg =(endOfInterval-beginingOfInterval)*uniformDistribution.nextDouble();
			
			if(uniformDistribution.nextDouble()*maxIntensity<
					kleinNishinaFormula(angleDeg,currentEnergy))
				angleOfScatteringDeg=Amount.valueOf(angleDeg,NonSI.DEGREE_ANGLE);
			
		}
		
		if(uniformDistribution.nextBoolean()==true)
			angleOfScatteringDeg=Amount.valueOf(360,NonSI.DEGREE_ANGLE)
								 .minus(angleOfScatteringDeg);
		
		return angleOfScatteringDeg;
	}
	
	/**
	 * Draws random angle, where the density of probability 
	 * 
	 * @return angle in degrees from Klein-Nishina distribution.
	 */
	public double randDouble(Amount<Energy> currentEnergy){
		double angleOfScatteringDeg = 0;
		
		maxIntensity=kleinNishinaFormula(0,currentEnergy);
		
		//algorytm von Neumanna
		while(angleOfScatteringDeg==0){
			
			double angleDeg =(endOfInterval-beginingOfInterval)
					*uniformDistribution.nextDouble();
			
			if(uniformDistribution.nextDouble()*maxIntensity
					< kleinNishinaFormula(angleDeg,currentEnergy))
				angleOfScatteringDeg=angleDeg;
		}
		
		if(uniformDistribution.nextBoolean()==true)
			angleOfScatteringDeg = 360 - angleOfScatteringDeg;
		return angleOfScatteringDeg;
	}
	
	/**
	 * @deprecated
	 * Evaluates probability density for specified angle and energy of incident photon
	 * 
	 * @param incidentEnergy  value of energy of incident photon
	 * @param angle which the probability density is returned for
	 * @return value of probability density that angle will be drawn from Klein-Nishina 
	 * Distribution
	 */
	public double kleinNishinaFormula(double incidentEnergy,double angle){
		
		double formulaValue=0;
		incidentEnergy=1.6*Math.pow(10,-16)*incidentEnergy;
		
		formulaValue=
				Math.pow(Results.re_SI, 2)*
				Math.pow(1/(1+incidentEnergy*(1-Math.cos(Math.toRadians(angle)))/
						(Results.mec2_KEV)), 2)*
				(1/(1+incidentEnergy*(1-Math.cos(Math.toRadians(angle)))/(Results.mec2_KEV))+
				(1+incidentEnergy*(1-Math.cos(Math.toRadians(angle)))/(Results.mec2_KEV))-
				Math.pow(Math.sin(Math.toRadians(angle)), 2));
		
		return formulaValue;
	}
	
	
	
	/**
	 * 
	 * Evaluates probability density for specified angle and energy of incident photon
	 * 
	 * @param angle which the probability density is returned for
	 * @param incidentEnergy  value of energy of incident photon
	 * @return value of probability density that angle will be drawn from Klein-Nishina
	 *  Distribution
	 */
	public static double kleinNishinaFormula(Amount<Angle> angle, Amount<Energy> incidEn) {
		// TODO Auto-generated method stub
		incidEn.to(NonSI.ELECTRON_VOLT);
		
		Amount<Dimensionless> ENERGY_RATIO = (Amount<Dimensionless>)
				incidEn.
				divide(	Results.me	).
				times(	1-Math.cos( angle.doubleValue(SI.RADIAN) )	).
				plus(	incidEn.divide(incidEn)	).to(Unit.ONE);
		
		Amount<Dimensionless> KleinNishinaFactor =
				(Amount<Dimensionless>) ((ENERGY_RATIO.inverse()).pow(2)).
				times(ENERGY_RATIO.inverse().plus(ENERGY_RATIO).
						minus(Amount.valueOf(
								Math.pow(	Math.sin( angle.doubleValue(SI.RADIAN) ), 2	)
									,Unit.ONE)
								)
				).to(Unit.ONE);
		
		return KleinNishinaFactor.doubleValue(Unit.ONE);
	};
	
	/**
	 * 
	 * Evaluates probability density for specified angle and energy of incident photon
	 * 
	 * @param angleDeg angle in degrees which the probability density is returned for
	 * @param incidentEnergy  value of energy of incident photon
	 * @return value of probability density that angle will be drawn from Klein-Nishina
	 * distribution
	 */
	public static double kleinNishinaFormula(double angleDeg, Amount<Energy> incidEn) {
		// TODO Auto-generated method stub
		incidEn.to(NonSI.ELECTRON_VOLT);
			
		Amount<Dimensionless> ENERGY_RATIO =
				(Amount<Dimensionless>)incidEn.divide(Results.me).
				times(	1-Math.cos( Math.toRadians(angleDeg) )	).
				plus(	incidEn.divide(incidEn)	).to(Unit.ONE);
		
		Amount<Dimensionless> KleinNishinaFactor =
				(Amount<Dimensionless>) ((ENERGY_RATIO.inverse()).pow(2)).
				times(ENERGY_RATIO.inverse().plus(ENERGY_RATIO).
						minus(Amount.valueOf(
								Math.pow(	Math.sin( Math.toRadians(angleDeg) ), 2	)
								,Unit.ONE)
							)
				).to(Unit.ONE);
		
		return KleinNishinaFactor.doubleValue(Unit.ONE);
	};
}
