/**
 * The packages contains: I18N .property files (English/Polish), icon .png file and theory description files.
 */
/**
 * @author Konrad Kobuszewski, Karolina Kulesz
 *
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.resources;