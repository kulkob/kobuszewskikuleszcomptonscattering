/**
 * The package contains classes for data and database management.
 */
/**
 * @author Konrad Kobuszewski
 *
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil;