package pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Energy;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

import org.jscience.physics.amount.Amount;
import org.jscience.physics.amount.Constants;

/**
 * This class contains static methods for calculations on drawn energies and angles 
 * to evaluate other parameters. Also can provide a container for drawn energies and angles.
 * 
 * @author Konrad Kobuszewski
 */
public class Results {
	//Fields
	private volatile Amount<Angle> photonAngleDeg;
	private volatile Amount<Angle> electronAngleDeg;
	private volatile Amount<Energy> incidentEnergyKeV;
	private volatile Amount<Energy> scatteredEnergyKeV;
	private volatile double gamma;
	
	//Dimensions
	public static Unit<Energy> KEV = SI.KILO(NonSI.ELECTRON_VOLT);

	//Physical constants
	public static Amount<Energy> me =
			(Amount<Energy>) Constants.me.times(Constants.c_square).to(KEV);
	public static double mec2_KEV =
			me.doubleValue(SI.KILO(NonSI.ELECTRON_VOLT));
	public static double c_SI =
			Constants.c.doubleValue(SI.METRES_PER_SECOND);
	public static double re_SI=2.817940*Math.pow(10,-15);
	//Source: en.wikipedia.org/wiki/Classical_electron_radius
	public static Amount<Length> re = Amount.valueOf(re_SI,SI.METER);
		
	/**
	 * Creates Results class.
	 * @param angle to be drawn.
	 * @param energy to be drawn.
	 */	
	public Results(Amount<Angle> anglePhDeg, Amount<Energy> inEnergyKeV) {
		// TODO Auto-generated constructor stub
		
		setPhotonAngleDeg(anglePhDeg);
		setIncidentEnergyKeV(inEnergyKeV);
		setElectronAngleDeg(countElcAngleDeg(inEnergyKeV, anglePhDeg));
		setScatteredEnergyKeV(countOutPhEnergyKeV(inEnergyKeV, anglePhDeg));
		setGamma(countGamma(inEnergyKeV, getScatteredEnergyKeV()));
		
	}
	
/********* Methods for evaluating scattering angles, energies, relativistic beta factor **********/
	/**
	 * This method calculates energy of photons after scattering on given angle
	 * using Compton formula.
	 * 
	 * @param inEnergyKeV - energy of incident photon
	 * @param anglePhDeg - angle which the photon scattered on
	 * @return outPhEnergy - energy of photon after scattering on anglePhDeg
	 */
	public static Amount<Energy> countOutPhEnergyKeV(
			  Amount<Energy> inEnergyKeV, Amount<Angle> anglePhDeg){	
	
		Amount<Energy> outPhEnergyKeV = 
			(inEnergyKeV).divide(
			(inEnergyKeV.divide(mec2_KEV))
			.doubleValue(SI.KILO(NonSI.ELECTRON_VOLT))
			*( 1 - Math.cos(	anglePhDeg.to(SI.RADIAN).doubleValue(SI.RADIAN)	) )
			+ 1	).to(KEV);
	
		return outPhEnergyKeV;
	}
	
	/**
	 * This method calculates energy of photons after scattering on given angle
	 * using Compton formula.
	 * 
	 * @param inEnergyKeV - energy of incident photon
	 * @param anglePhDeg - angle which the photon scattered on
	 * @return outPhEnergy - energy of photon after scattering on anglePhDeg
	 */
	public static Amount<Energy> countOutPhEnergyKeV(
			  Amount<Energy> inEnergyKeV, double anglePhDeg){	
	
		Amount<Energy> outPhEnergyKeV = 
			(inEnergyKeV).divide(
			(inEnergyKeV.divide(mec2_KEV))
			.doubleValue(SI.KILO(NonSI.ELECTRON_VOLT))
			*( 1 - Math.cos( Math.toRadians(anglePhDeg)	) )
			+ 1	).to(KEV);
	
		return outPhEnergyKeV;
	}
	
	/**
	 * Calculates angle which the electron is reflected after scattering.
	 * 
	 * @param inEnergyKeV - energy of incident photon
	 * @param anglePhDeg - angle which the photon is scattered on
	 * @return angleElcDeg - relativistic expression for angle which the electron
	 * is reflected in impact with given parameters
	 */
	public static Amount<Angle> countElcAngleDeg(
				  Amount<Energy> inEnergyKeV, Amount<Angle> anglePhDeg){	
		
		Amount<Angle> angleElcDeg = Amount.valueOf(
				Math.atan( 1/(
				( 1 + (inEnergyKeV).doubleValue(KEV) / mec2_KEV		)
				*Math.tan( ((anglePhDeg.divide(2)).to(SI.RADIAN)).doubleValue(SI.RADIAN) )
				)
			), SI.RADIAN );
		
		return angleElcDeg.to(NonSI.DEGREE_ANGLE);
	}
	
	/**
	 * Calculates angle which the electron is reflected after scattering.
	 * 
	 * @param inEnergyKeV - energy of incident photon
	 * @param anglePhDeg - angle which the photon is scattered on
	 * @return angleElcDeg - relativistic expression for angle which the electron
	 * is reflected in impact with given parameters
	 */
	public static double countElcAngleDeg(
				  Amount<Energy> inEnergyKeV, double anglePhDeg){	
		
		Amount<Angle> angleElcDeg = Amount.valueOf(
				Math.atan( 1/(
				( 1 + (inEnergyKeV).doubleValue(KEV) / mec2_KEV		)
				*Math.tan( Math.toRadians(anglePhDeg/2) )
				)
			), SI.RADIAN );
		
		return angleElcDeg.doubleValue(NonSI.DEGREE_ANGLE);
	}
	
	/**
	 * @deprecated
	 * This method counts double value of relativistic beta factor.
	 *
	 * @param inEnergyKeV
	 * @param anglePhDeg
	 * @param outEnergyKeV
	 * @param angleElcDeg
	 * @return beta - relativistic beta factor.
	 */
	public static double countBeta(
			 Amount<Energy> inEnergyKeV, Amount<Angle> anglePhDeg,
			 Amount<Energy> outEnergyKeV, Amount<Angle> angleElcDeg){
		
		double beta = Math.sqrt(
				1 + Math.pow(me.doubleValue(NonSI.ELECTRON_VOLT),2)/(
				1 + Math.pow(inEnergyKeV.doubleValue(NonSI.ELECTRON_VOLT)/
						outEnergyKeV.doubleValue(NonSI.ELECTRON_VOLT),2) 
				- 2*Math.cos( anglePhDeg.doubleValue(NonSI.DEGREE_ANGLE) )*
				inEnergyKeV.doubleValue(NonSI.ELECTRON_VOLT)/
				outEnergyKeV.doubleValue(NonSI.ELECTRON_VOLT))
				);
		
		return beta;
	}
	
	/**
	 * This method evaluates relativistic gamma factor.
	 * 
	 * @param inEnergyKeV - energy of incident photon
	 * @param anglePhDeg - angle which the photon is scattered on
	 * @return gamma - relativistic gamma factor, necessary to count electron's velocity
	 */
	public static double countGamma(
			Amount<Energy> inEnergyKeV, Amount<Energy> outEnergyKeV){
		
		return	( (inEnergyKeV.minus(outEnergyKeV))
				.doubleValue(KEV)
				/me.doubleValue(KEV) )	+ 1;
	}
	
/*********************************** Setters and getters *****************************************/
	
	public Amount<Angle> getPhotonAngleDeg() {
		return photonAngleDeg;
	}

	public void setPhotonAngleDeg(Amount<Angle> photonAngleDeg) {
		this.photonAngleDeg = photonAngleDeg;
	}

	public Amount<Angle> getElectronAngleDeg() {
		return electronAngleDeg;
	}

	public void setElectronAngleDeg(Amount<Angle> electronAngleDeg) {
		this.electronAngleDeg = electronAngleDeg;
	}

	public Amount<Energy> getIncidentEnergyKeV() {
		return incidentEnergyKeV;
	}

	public void setIncidentEnergyKeV(Amount<Energy> incidentEnergyKeV) {
		this.incidentEnergyKeV = incidentEnergyKeV;
	}

	public Amount<Energy> getScatteredEnergyKeV() {
		return scatteredEnergyKeV;
	}

	public void setScatteredEnergyKeV(Amount<Energy> scatteredEnergyKeV) {
		this.scatteredEnergyKeV = scatteredEnergyKeV;
	}
	
	public double getGamma() {
		return gamma;
	}

	public void setGamma(double gamma) {
		this.gamma = gamma;
	}
/********************************************** End **********************************************/
}
