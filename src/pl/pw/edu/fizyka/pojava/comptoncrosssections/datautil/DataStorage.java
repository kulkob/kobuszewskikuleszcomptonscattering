package pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jscience.physics.amount.Amount;

/**
 * This class collects distribution of energy of incident photons.
 * Field energyDist - an array representing distribution of energy of incident photons.
 * Some real distributions are given and can be used in program by specified methods.
 * Contains value of energetic channel and its relative intenstity.
 * @author Konrad Kobuszewski
 */
public class DataStorage {
	
	volatile ArrayList<ImmutablePair<Amount<Energy>,Double>> energyDist = null;
	volatile double maxIntensity=1;
	public volatile ResourceBundle messages;
	
	Unit<Energy> KEV = SI.KILO(NonSI.ELECTRON_VOLT);
		
	public DataStorage(ResourceBundle messages) {
		// TODO Auto-generated constructor stub
		energyDist = new ArrayList<ImmutablePair<Amount<Energy>,Double>>();
		this.messages = messages;
	}

/********************************** Class utility functions **************************************/
	
	/**
	 * Returns distribution of energy of incident photons.
	 * 
	 * @return ArrayList of ImmutablePairs, left field of a pair contains (Amount/Energy)
	 * value of i-th energetic channel and right field contains relative intensity (double,
	 * not greater than one).
	 */
	public ArrayList<ImmutablePair<Amount<Energy>, Double>> getEnergyDistrib() {
		// TODO Auto-generated method stub
		return energyDist;
	}
	
	/**
	 * Removes data with specified index.
	 * 
	 * @param index index of data to be removed
	 */
	public void removeChannel(int index){
		energyDist.remove(index);
	}
	
	/**
	 * Returns ImmutablePair representing channel of index given by user.
	 * 
	 * @param index index of channel which will be returned
	 * @return chosen ImmutablePair representing channel
	 */
	public ImmutablePair<Amount<Energy>, Double> getChannel(int index){
		return energyDist.get(index);
	}
	
	/**
	 * Returns string describing energy of channel of index given by user.
	 * 
	 * @param index index of channel which will be returned
	 * @return string describing energy of channel
	 */
	public String getChannelLabel(int index){
		return getChannel(index).getLeft().toString();
	}
	
	/**
	 * Returns value of energy corresponding to channel with specified index.
	 * 
	 * @param index - index of channel
	 * @return double value of energy of channel with index specified by param
	 */
	public double getChannelEnDouble(int index){
		return getChannel(index).getLeft().doubleValue(KEV);
	}
	
	/**
	 * Returns relative intensity of channel with specified index.
	 * 
	 * @param index - index of channel
	 * @return double describing relative intensity of channel with index specified by param
	 */
	public double getChannelIntensity(int index){
		return getChannel(index).getRight();
	}
	
	/**
	 * Returns number of channels in distribution.
	 * 
	 * @return size of arraylist containing distribution of energy.
	 */
	public int size() {
		return energyDist.size();
	}
	
	/**
	 * Clears all data and incident photon's energy distribution in DataStorage instance.
	 */
	public void clearDist(){
		energyDist.clear();
	}

/******************************* Defined distributions of Energy *********************************/
	
	/**
	 * Adds simple channel with maximal relative intensity.
	 * 
	 * @param energyValue - value of energy of this channel (in keV)
	 */
	public void addSimpleChannel(double energyValue){
		clearDist();
		energyDist.add(size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(energyValue, KEV), 1.0) );
		JOptionPane.showMessageDialog(null,
			messages.getString("DataStorage.simpleChannel")+" "+energyDist.get(0).getLeft()
			.doubleValue(KEV)+" keV !");
	}
	
	/**
	 * Adds Molibdenium K lines to the Energies Distribution.
	 * Source: 	Atomic and nuclear physics, 
	 * LEYBOLD Physics Leaflets, 
	 * "Fine structure of the characteristic x-radiation of a molybdenum anode"
	 */
	public void addMoDistrib(){
		clearDist();
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(17.44, KEV), 1.0)	);
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(19.60, KEV), 0.17)	);
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(19.97, KEV), 0.027));
		
		JOptionPane.showMessageDialog(null,messages.getString("DataStorage.Mo"));
	}
	
	/**
	 * Adds Tungsten K lines to the Energies Distribution.
	 * Sources: http://www.tc.umn.edu/~watan016/Production_of_x-rays_RTT.pdf (Values of lines),
	 *  http://www.kns.org/jknsfile/v42/JK0420426.pdf (intensity of lines)
	 */
	public void addWfDistrib(){
		clearDist();
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(57.97, KEV), 0.672) );
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(59.31, KEV), 1.000) );
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(67.23, KEV), 0.299) );
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(69.09, KEV), 0.006) );
		
		JOptionPane.showMessageDialog(null,messages.getString("DataStorage.Wf"));
	}
	
	/**
	 * Adds value of Cs-decay gamma rays' energy to the Energies Distribution.
	 * Source: http://en.wikipedia.org/wiki/Caesium#Isotopes
	 */
	public void addCsDistrib(){
		clearDist();
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(621.7, KEV), 1.0)	);
		
		JOptionPane.showMessageDialog(null,	messages.getString("DataStorage.Cs"));
	}
	
	/**
	 * Adds value of Cs-decay gamma rays' energies to the Energies Distribution.
	 * Source: http://en.wikipedia.org/wiki/Cobalt-60#mediaviewer/File:60Co_gamma_spectrum_energy.png
	 */
	public void addCoDistrib(){
		clearDist();
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(1173.2, KEV), 1.0)	);
		energyDist.add(energyDist.size(),
				new ImmutablePair<Amount<Energy>,Double>( Amount.valueOf(1332.5, KEV), 0.869));
		
		JOptionPane.showMessageDialog(null,messages.getString("DataStorage.Co"));
	}
	
/**************************************** The End ************************************************/

}