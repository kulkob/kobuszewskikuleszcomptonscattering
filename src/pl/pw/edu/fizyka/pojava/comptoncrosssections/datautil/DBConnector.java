package pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.List;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jscience.physics.amount.Amount;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;

/**
 * Class for operations with database. A database is required due to limitation
 * of JVM memory, so to collect data for histograms and tables a database is the
 * most suitable option.
 * 
 * @author Konrad Kobuszewski
 *
 */
public class DBConnector {
	static Connection conn = null;
	
	/**
	 * Constructor of class DBConnector. Creates an instance of this class,
	 * connects with DataHolder database and creates table or clears results.
	 * @throws SQLException 
	 * 
	 */
	public DBConnector() throws SQLException {
		try {
			conn = DriverManager.getConnection(
					"jdbc:h2:DataHolder", "sa", "");
		} catch(SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();;
		} 
		System.out.println("Database connected!");
	}

/************************************* DB utility functions **************************************/	

	/**
	 * This method creates database to holding results calculated in the background.
	 * 
	 * @param conn - Connetion to DataHolder.h2.db
	 * @throws SQLException
	 */
	public void createTable() throws SQLException{
		
		Statement statement = conn.createStatement();
		statement.executeUpdate("DROP TABLE IF EXISTS DataHolder;");
		statement.execute("CREATE TABLE DataHolder(" +
				"ID INTEGER," +
				"INCIDENT_EN_PH OBJECT, " +
				"SCAT_EN_PH OBJECT," +
				"SCAT_ANG_PH OBJECT," +
				"SCAT_ANG_ELC OBJECT," +
				"GAMMA DOUBLE," + ");" );
		
	}
	
	/**
	 * This method creates database for holding computation results from animation data. 
	 * 
	 * @author Karolina Kulesz
	 * @param title - string representing headline of new Table in database.
	 * @throws SQLException
	 */	
	public void createTable(String title) throws SQLException{
		
		Statement statement = conn.createStatement();
		statement.executeUpdate("DROP TABLE IF EXISTS "+title+";");
		statement.execute("CREATE TABLE "+title+"(" +
				"ID INTEGER," +
				"INCIDENT_EN_PH OBJECT, " +
				"SCAT_EN_PH OBJECT," +
				"SCAT_ANG_PH OBJECT," +
				"SCAT_ANG_ELC OBJECT," +
				"GAMMA DOUBLE," + ");" );
	}
	
	
	/**
	 * This method inserts one record of data into DataHolder database.
	 * 
	 * @param id - specific nummer of data record in db 
	 * @param inEnergyKeV - energy of incident photon in event described in this record (Amount)
	 * @param anglePhDeg - angle of scattering of photon in event described in this record (Amount)
	 * @throws SQLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void insertData( int id, Amount<Energy> inEnergyKeV,
					 Amount<Angle> anglePhDeg) throws SQLException{
		
		PreparedStatement statement =
				conn.prepareStatement("INSERT INTO DataHolder VALUES(?,?,?,?,?,?)");
		
		Amount<Energy> outEnergyKeV = Results.countOutPhEnergyKeV(inEnergyKeV, anglePhDeg);
		Amount<Angle> angleElcDeg = Results.countElcAngleDeg(inEnergyKeV, anglePhDeg);
		double gamma = Results.countGamma(inEnergyKeV, outEnergyKeV);
		
		statement.setInt(1, id);
		statement.setObject(2, inEnergyKeV);
		statement.setObject(3, outEnergyKeV);
		statement.setObject(4, anglePhDeg);
		statement.setObject(5, angleElcDeg);
		statement.setDouble(6, gamma);
		
		statement.executeUpdate();
		
	}
	
	/**
	 * (Suitable only for tests - shows data in stdout)
	 * @deprecated
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	@SuppressWarnings("unchecked")
	public void showData() throws SQLException{
		
		PreparedStatement statement = null;
		try {
			
			statement = conn.prepareStatement(
					"SELECT * FROM `DataHolder` " );
			statement.execute();
			
			ResultSet rs = statement.getResultSet();		
			ResultSetMetaData md  = rs.getMetaData();
			
			for (int ii = 1; ii <= md.getColumnCount(); ii++){
				System.out.print(MessageFormat.format("{0}|", md.getColumnName(ii)));	
			}
			
			System.out.println();
			int kk=0;
			while (rs.next()) {
				kk++;
				System.out.print(kk+". ");
				for (int ii = 1; ii <= md.getColumnCount(); ii++){
					System.out.print(MessageFormat.format("{0}|", rs.getObject(ii)));
				}
				System.out.println();
				System.out.println(
						((Amount<Energy>) rs.getObject(2)).doubleValue(Results.KEV));
			}
		} finally {
			
		}
		
	}
	
	/**
	 * Saves database in location chosen by JFileChooser as h2.db database.
	 */
	public void saveAsDB(){
		
		try {
			SaveDB save = new SaveDB();
			save.execute();
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Saves data from database in location chosen by JFileChooser as .txt file.
	 */
	public void saveAsTxt(){
		
		try {
			SaveAsTxt save = new SaveAsTxt();
			save.execute();
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Closes connection to DataHolder.h2.db .
	 */
	public void connclose() {
		// TODO Auto-generated method stub
		try {
			if(conn != null)
				conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Database disconnected!");
	}
		
/******************************* DataSet for histogram methods ***********************************/
	
	public HistogramDataset getPhAngles(){
		DBGetPhAng getDB = new DBGetPhAng();
		getDB.execute();
		return getDB.getHistData();
	}
	
	public HistogramDataset getElcAngles(){
	
		DBGetElcAng getDB = new DBGetElcAng();
		getDB.execute();
		return getDB.getHistData();
	}
	
	public DefaultCategoryDataset getIncidentPhEnergies(){
		DBGetIncidentPhEnergies getDB = new DBGetIncidentPhEnergies();
		getDB.execute();
		return getDB.getHistData();
	}
	
	public HistogramDataset getScatPhEnergies(){
		DBGetScatPhEnergies getDB = new DBGetScatPhEnergies();
		getDB.execute();
		return getDB.getHistData();
	}
	

/**************************** SwingWorkers for operations on DB **********************************/
	
	/**
	 * This class provides interface between program and db to call data.
	 * Uses thread independent from EDT.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class DBGetPhAng extends SwingWorker<Void, Integer> {
		
		double[] array = null;
		String sql = "SCAT_ANG_PH";
		HistogramDataset dataset = null;
				
		DBGetPhAng( ){
			super();
			dataset = new HistogramDataset();
			
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			try{
				Statement statement = conn.createStatement(
		                ResultSet.TYPE_SCROLL_SENSITIVE,
		                ResultSet.CONCUR_UPDATABLE);
				
				ResultSet rs = statement.executeQuery("SELECT "+sql+" FROM `DataHolder`");
				ResultSetMetaData md  = rs.getMetaData();
				
				if(  sql.equals(md.getColumnLabel(1))  ){
					rs.last();
					if(rs.isLast()){
						array = new double[rs.getRow()];
					}
					rs.first();
					int counter = 0;
					while(rs.next()){
						array[counter] = getAmount(rs)
								.doubleValue(NonSI.DEGREE_ANGLE);
						counter++;
						publish(counter);
					}
					dataset.addSeries("Photons' angles counts", array, 72);
				}
				else
					throw new SQLException("Mismatching colummn labels!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
						
			return null;
		}

		@Override
		protected void process(List<Integer> progress){
			for(int prog : progress){
				setProgress(100-prog/array.length);
			}
		}
				
		@SuppressWarnings("unchecked")
		private Amount<Angle> getAmount(ResultSet rs) throws SQLException {
			return ((Amount<Angle>) rs.getObject(1)).to(NonSI.DEGREE_ANGLE);
		}
		
		public double[] getArray(){
			return array;
		}
				
		public HistogramDataset getHistData(){
			return dataset;
		}
	}
	
	/**
	 * This class provides interface between program and db to call data.
	 * Uses thread independent from EDT.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class DBGetElcAng extends SwingWorker<Void, Void> {
		
		double[] array = null;
		String sql = "SCAT_ANG_ELC";
		HistogramDataset dataset = null;
		
		DBGetElcAng(){
			super();
			dataset = new HistogramDataset();
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			try{
				
				Statement statement = conn.createStatement(
		                ResultSet.TYPE_SCROLL_SENSITIVE,
		                ResultSet.CONCUR_UPDATABLE);
				
				ResultSet rs = statement.executeQuery("SELECT "+sql+" FROM `DataHolder`");
				ResultSetMetaData md  = rs.getMetaData();
				
				if(  sql.equals(md.getColumnLabel(1))  ){
					rs.last();
					if(rs.isLast()){
						array = new double[rs.getRow()];
					}
					rs.first();
					int counter = 0;
					while(rs.next()){
						array[counter] = getAmount(rs)
								.doubleValue(NonSI.DEGREE_ANGLE);
						counter++;
					}
					dataset.addSeries("Electrons' angles counts", array, 72);
				}
				else
					throw new SQLException("Mismatching colummn labels!");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
			
			return null;
		}
		
		@SuppressWarnings("unchecked")
		private Amount<Angle> getAmount(ResultSet rs) throws SQLException {
			return ((Amount<Angle>) rs.getObject(1)).to(NonSI.DEGREE_ANGLE);
		}
		
		public double[] getArray(){
			return array;
		}
		
		public HistogramDataset getHistData(){
			return dataset;
		}
	}
	
	/**
	 * This class provides interface between program and db to call data
	 * and creates Dataset for BarChart showing incident energy distribution.
	 * Uses thread independent from EDT.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class DBGetIncidentPhEnergies extends SwingWorker<Void, Void> {
		
		double[] array = null;
		String sql = "INCIDENT_EN_PH";
		DefaultCategoryDataset dataset = null;
		
		/**
		 * This class provides interface between program and db to call data.
		 * Uses thread independent from EDT.
		 * 
		 * @author Konrad Kobuszewski
		 *
		 */
		DBGetIncidentPhEnergies(){
			super();
			dataset = new DefaultCategoryDataset();
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			String theoreticalSeries = "Theoretical values";
			String experimentalSeries = "Experimental values";
			
			String[] categories = new String[Main.storage.size()];
			for(int ii=0; ii < Main.storage.size(); ii++){
				categories[ii] = Main.storage.getChannelLabel(ii);
			}
			
			double[] counts = new double[Main.storage.size()];
			for(int ii=0; ii < Main.storage.size(); ii++){
				counts[ii] = 0;
			}
			
			try{
				 
				Statement statement = conn.createStatement(
		                ResultSet.TYPE_SCROLL_SENSITIVE,
		                ResultSet.CONCUR_UPDATABLE);
				
				ResultSet rs = statement.executeQuery("SELECT "+sql+" FROM `DataHolder`");
				ResultSetMetaData md  = rs.getMetaData();
				
				if(  sql.equals(md.getColumnLabel(1))  ){
					rs.last();
					if(rs.isLast()){
						array = new double[rs.getRow()];
					}
					rs.first();
					int counter = 0;
					while(rs.next()){
						array[counter] = getAmount(rs)
								.doubleValue(Results.KEV);
						for(int ii=0; ii < Main.storage.size(); ii++){
							if(array[counter] == Main.storage.getChannelEnDouble(ii))
								counts[ii] +=1;
						}
						counter++;
					}
					//creating Dataset for BarChart
					for(int ii=0; ii < Main.storage.size(); ii++){
						dataset.addValue( counts[ii]/counter,
										  experimentalSeries,
										  categories[ii]);
					}
					for(int ii=0; ii < Main.storage.size(); ii++){
						dataset.addValue(Main.storage.getChannelIntensity(ii), theoreticalSeries,
								categories[ii]);
					}
				}
				else
					throw new SQLException("Mismatching colummn labels!");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
			
			return null;
		}
		
		@SuppressWarnings("unchecked")
		private Amount<Energy> getAmount(ResultSet rs) throws SQLException {
			return ((Amount<Energy>) rs.getObject(1)).to(Results.KEV);
		}
		
		public double[] getArray(){
			return array;
		}
		
		public DefaultCategoryDataset getHistData(){
			return dataset;
		}
	}
	
	/**
	 * This class provides interface between program and db to call data.
	 * Uses thread independent from EDT.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class DBGetScatPhEnergies extends SwingWorker<Void, Void> {
		
		double[] array = null;
		String sql = "SCAT_EN_PH";
		HistogramDataset dataset = null;
		
		DBGetScatPhEnergies(){
			super();
			dataset = new HistogramDataset();
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			try{
				
				Statement statement = conn.createStatement(
		                ResultSet.TYPE_SCROLL_SENSITIVE,
		                ResultSet.CONCUR_UPDATABLE);
				
				ResultSet rs = statement.executeQuery("SELECT "+sql+" FROM `DataHolder`");
				ResultSetMetaData md  = rs.getMetaData();
				
				if(  sql.equals(md.getColumnLabel(1))  ){
					rs.last();
					if(rs.isLast()){
						array = new double[rs.getRow()];
					}
					rs.first();
					int counter = 0;
					while(rs.next()){
						array[counter] = getAmount(rs)
								.doubleValue(Results.KEV);
						counter++;
					}
					dataset.addSeries("Energies of scattered photons", array, 100);
				}
				else
					throw new SQLException("Mismatching colummn labels!");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
			
			return null;
		}
		
		@SuppressWarnings("unchecked")
		private Amount<Energy> getAmount(ResultSet rs) throws SQLException {
			return ((Amount<Energy>) rs.getObject(1)).to(Results.KEV);
		}
		
		public double[] getArray(){
			return array;
		}
		
		public HistogramDataset getHistData(){
			return dataset;
		}
	}
	
	/**
	 * This class provides methods for saving database in chosen location.
	 * Uses thread independent form EDT.
	 * New instance of this class calls JFileChooser to choose path and name
	 * for new .h2.db database.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class SaveDB extends SwingWorker<Void, Void>{
		
		Connection newConn = null;
		JFileChooser fc;
		String filePath = "";
		String name = "compton";
		
		/**
		 * Constructor of class saveDB.
		 * 
		 * @throws SQLException
		 * @throws IOException
		 */
		SaveDB() throws SQLException, IOException{
			super();
			
			fc = new JFileChooser();
			fc.setDialogTitle("Save as database");
			if(fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION){
				filePath = fc.getSelectedFile().getPath();
				name = fc.getSelectedFile().getName();
			}
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			
			newConn = DriverManager.getConnection(
					"jdbc:h2:"+filePath, "sa", "");
			
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * FROM `DataHolder`");
			statement.execute();
			
			ResultSet rs = statement.getResultSet();
			ResultSetMetaData md  = rs.getMetaData();
			
			PreparedStatement newStatement = newConn.prepareStatement(
					"INSERT INTO "+name+" VALUES(?,?,?,?,?,?)");
			
			while(rs.next()){
				for (int ii =1; ii <= md.getColumnCount(); ii++){
					newStatement.setObject(ii, rs.getObject(ii));
				}
				newStatement.execute();
			}
			
			return null;
		}
		
	}
	
	/**
	 * This class provides methods for saving data from database in chosen location.
	 * Uses thread independent form EDT.
	 * New instance of this class calls JFileChooser to choose path and name
	 * for new .txt file.
	 * 
	 * @author Konrad Kobuszewski
	 *
	 */
	public class SaveAsTxt extends SwingWorker<Void, Void> {
		
		JFileChooser fc;
		File file;
		String name = "";
		
		SaveAsTxt() throws SQLException, IOException{
			super();
			
			fc = new JFileChooser();
			fc.setDialogTitle("Save as .txt");
			if(fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION){
				file = fc.getSelectedFile().getAbsoluteFile();
				name = fc.getSelectedFile().getName();
				System.out.println(file);
			}
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * FROM `DataHolder`");
			statement.execute();
			
			ResultSet rs = statement.getResultSet();
			ResultSetMetaData md = rs.getMetaData();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			while(rs.next()){
				for(int ii=1; ii <= md.getColumnCount(); ii++){
					writer.write( rs.getObject(ii).toString() );
					if(ii != md.getColumnCount())
						writer.write("\t");
				}
				writer.write("\n");
			}
			
			if(writer != null)
				writer.close();
			
			return null;
		}
		
	}
	
/******************************************** End ************************************************/
	
}