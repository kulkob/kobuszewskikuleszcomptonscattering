package pl.pw.edu.fizyka.pojava.comptoncrosssections.unittests;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;
import org.junit.Before;
import org.junit.Test;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.Results;

/**
 * !!!	Does not work	!!!
 * @author Konrad Kobuszewski
 *
 */
public class DBConnectorTest {

	@Before
	public void getConnection() throws SQLException{
		
		
	}
	
	@SuppressWarnings({ "unchecked" })
	@Test
	public void returnTypeTest() throws SQLException {
		
		int id = 1;
		Amount<Energy> inEnergyKeV = Amount.valueOf(10,Results.KEV);
		Amount<Angle> anglePhDeg = Amount.valueOf(1.0,SI.RADIAN);
		
		Connection conn = DriverManager.getConnection(
				"jdbc:h2:TestDB", "sa",	"");
		
		Statement statement = conn.createStatement();
		statement.executeUpdate("DROP TABLE IF EXISTS TestDB;");
		statement.execute("CREATE TABLE TestDB(" +
				"ID INTEGER," +
				"INCIDENT_EN_PH OBJECT, " +
				"SCAT_ANG_PH OBJECT)");
		
		PreparedStatement statement1 = conn.prepareStatement("INSERT INTO TestDB VALUES(?,?,?)");
		statement1.setInt(1, id);
		statement1.setObject(2, inEnergyKeV);
		statement1.setObject(3, anglePhDeg);
		statement1.execute();
		
		PreparedStatement statement2 = conn.prepareStatement("SELECT * FROM `TestDB`");
		statement2.execute();
		
		ResultSet rs = statement2.getResultSet();
		ResultSetMetaData md  = rs.getMetaData();
		
		for(int ii=1; ii <= md.getColumnCount(); ii++){
			rs.getObject(ii);
		}
		
		double dbEnergy =
		((Amount<Energy>) rs.getObject(2)).doubleValue(Results.KEV);//Dane nie sa dostepne!!! Nie wiem dlaczego???
		double dbAngle =
		((Amount<Angle>) rs.getObject(3)).doubleValue(NonSI.DEGREE_ANGLE);
		
		assertEquals("Nie zgadza si� warto�� id zwr�cona przez db!",
				id , rs.getInt(0));
		assertEquals("Nie zgadza si� warto�� energii zwr�cona przez db!",
				10.0 , dbEnergy,0.0001);
		assertEquals("Nie zgadza si� warto�� kata zwr�cona przez db!",
				1.0 , dbAngle,0.0001);
		
		conn.close();
		
	}
}
