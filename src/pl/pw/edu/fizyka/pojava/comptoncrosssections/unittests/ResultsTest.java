package pl.pw.edu.fizyka.pojava.comptoncrosssections.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;

import org.jscience.physics.amount.Amount;
import org.junit.Test;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.Results;

/**
 * Testing calculations in static methods in class Results.
 * 
 * @author Konrad Kobuszewski
 *
 */
public class ResultsTest {
	
	@Test
	public void countElcAngleTest(){
		for(int jj=0; jj<10; jj++){
			Amount<Energy> inEnergyKeV = Amount.valueOf(Math.exp(jj),Results.KEV);
			for(int ii=0; ii<18; ii++){
				Amount<Angle> anglePhDeg = Amount.valueOf(ii*10, NonSI.DEGREE_ANGLE);
				assertEquals("Amount and double value does not equal!",
						Results.countElcAngleDeg(inEnergyKeV, anglePhDeg)
						.doubleValue(NonSI.DEGREE_ANGLE),
						Results.countElcAngleDeg(inEnergyKeV,
								anglePhDeg.doubleValue(NonSI.DEGREE_ANGLE)),
						0.0001);
				assertTrue("Electron's angle greater than 90 degress!",
						Results.countElcAngleDeg(inEnergyKeV, anglePhDeg)
						.doubleValue(NonSI.DEGREE_ANGLE) < 90);
				assertTrue("Electron's angle greater than 90 degress!",
						Results.countElcAngleDeg(inEnergyKeV, anglePhDeg)
						.doubleValue(NonSI.DEGREE_ANGLE) > -90);
			}
		}
		
	}
	
	@Test
	public void countOutPhEnergyTest() {
		for(int jj=0; jj<10; jj++){
			Amount<Energy> inEnergyKeV = Amount.valueOf(Math.exp(jj),Results.KEV);
			for(int ii=0; ii<18; ii++){
				double angleDeg = ii*10;
				Amount<Angle> angleAmount = Amount.valueOf(angleDeg,NonSI.DEGREE_ANGLE);
				assertEquals("Amount and double value does not equal!",
						Results.countOutPhEnergyKeV(inEnergyKeV, angleDeg).doubleValue(Results.KEV),
						Results.countOutPhEnergyKeV(inEnergyKeV, angleAmount).doubleValue(Results.KEV),
						0.0001);
				assertTrue("Energy of scattered photon greater than incident!",
						inEnergyKeV.doubleValue(Results.KEV) >=
						Results.countOutPhEnergyKeV(inEnergyKeV, angleAmount).doubleValue(Results.KEV));
			}
		}
	}
	
	@Test
	public void countGammaTest(){
		Amount<Energy> zero = Amount.valueOf(0,Results.KEV);
		for(int ii=0; ii<20; ii++){
			
			Amount<Energy> E1 = Amount.valueOf(ii,Results.KEV);
			Amount<Energy> E2 = Amount.valueOf(ii*10,Results.KEV);
			
			if(ii == 0){
				assertEquals("gamma values missmatch for En delta"+ii*9,
						Results.countGamma(E1, zero),Results.countGamma(E2, zero),0.0001);
				assertEquals("wronng gamma value for delta zero",
						Results.countGamma(E1, zero),1,0.0001);
			}
			else{
				assertTrue("gamma values missmatch for En delta"+ii*9,
						Results.countGamma(E1, zero) < Results.countGamma(E2, zero));
				assertTrue("gamma value lower than 1 for En delta"+ii*9,
						Results.countGamma(E2, zero)>1);
			}
		}
	}

}
