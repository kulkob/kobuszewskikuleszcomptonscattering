package pl.pw.edu.fizyka.pojava.comptoncrosssections.unittests;

import static org.junit.Assert.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javax.measure.unit.UnitFormat;

import org.jscience.physics.amount.Amount;
import org.jscience.physics.amount.Constants;
import org.junit.Before;
import org.junit.Test;

/**
 * Testing method for evaluating Klein-Nishina Formula.
 * 
 * @author Konrad Kobuszewski
 */
public class KleinNishinaTest {
	
	@Before
	public void addMoreUnits() throws Exception {
        // check the Unit class for more methods: plus, times, divide, inverse
		Unit<Energy> KEV = (Unit<Energy>) SI.KILO(NonSI.ELECTRON_VOLT);	
		UnitFormat unitFormat = UnitFormat.getInstance();
		unitFormat.alias(KEV, "keV");
    }
	
	
	/**
	 * This test checks if the density of probability is symmetric due to 180 degrees
	 */
	@Test
	public void symmetryTest() {
		@SuppressWarnings("unchecked")
		Unit<Dimensionless> nonDim = (Unit<Dimensionless>) NonSI.ELECTRON_VOLT.divide(NonSI.ELECTRON_VOLT);
		
		for (int jj=0; jj<8; jj++){
			
			Amount<Energy> inputEn = Amount.valueOf(jj*Math.pow(10,6-jj), SI.KILO(NonSI.ELECTRON_VOLT));
			
			for(int ii = 0; ii < 18; ii++){
				
				Amount<Angle> angleii = Amount.valueOf(ii*10, NonSI.DEGREE_ANGLE);
				Amount<Angle> anglei = Amount.valueOf(360-ii*10, NonSI.DEGREE_ANGLE);
				double KNValue1 = KleinNishinaFormula(angleii, inputEn).doubleValue(nonDim);
				double KNValue2 = KleinNishinaFormula(anglei, inputEn).doubleValue(nonDim);
				
				assertEquals("Dla energii "+inputEn+" k�ta "+angleii+" nie zgadza si�!",KNValue1,KNValue2,0.0001);
			}
		}
	}
	
	/**
	 * This test checks if the highest value of Klein-Nishina Formula is received for 0 degrees
	 */
	@Test
	public void highestValueTest(){
		@SuppressWarnings("unchecked")
		Unit<Dimensionless> nonDim = (Unit<Dimensionless>) NonSI.ELECTRON_VOLT.divide(NonSI.ELECTRON_VOLT);
		
		for (int jj=0; jj<8; jj++){
			
			Amount<Energy> inputEn = Amount.valueOf(Math.pow(10,6-jj), SI.KILO(NonSI.ELECTRON_VOLT));
			Amount<Angle> zeroAng = Amount.valueOf(0, NonSI.DEGREE_ANGLE);
			double highestValue = KleinNishinaFormula(zeroAng, inputEn).doubleValue(nonDim);
			
			for(int ii = 1; ii < 18; ii++){
				
				Amount<Angle> angleii = Amount.valueOf(ii*10, NonSI.DEGREE_ANGLE);
				double KNValue = KleinNishinaFormula(angleii, inputEn).doubleValue(nonDim);
				
				assertTrue("Dla energii "+inputEn+" k�ta "+angleii+" nie zgadza si�!", highestValue > KNValue);
				
			}
		}
	}
	
	@Test
	public void zeroValueTest(){
				
		for (int jj=0; jj<8; jj++){
			
			Amount<Energy> inputEn1 = Amount.valueOf(Math.pow(10,6-jj), SI.KILO(NonSI.ELECTRON_VOLT));
			Amount<Energy> inputEn2 = Amount.valueOf(Math.pow(10,6-jj), SI.KILO(NonSI.ELECTRON_VOLT));
			Amount<Angle> zeroAng = Amount.valueOf(0, NonSI.DEGREE_ANGLE);
			assertEquals( "Wrong crosssection at zero angle!",
					KleinNishinaFormula(zeroAng, inputEn1).doubleValue(Unit.ONE),
					KleinNishinaFormula(zeroAng, inputEn2).doubleValue(Unit.ONE),
					0.001);
		}
	}
	
	public static Amount<Dimensionless> KleinNishinaFormula(Amount<Angle> angle, Amount<Energy> incidEn) {
		// TODO Auto-generated method stub
		incidEn.to(NonSI.ELECTRON_VOLT);
				
		@SuppressWarnings("unchecked")
		Amount<Energy> me = (Amount<Energy>) Constants.me.times(Constants.c_square);
		
		@SuppressWarnings("unchecked")
		Amount<Dimensionless> ENERGY_RATIO = (Amount<Dimensionless>)
				incidEn.
				divide(	me.to(NonSI.ELECTRON_VOLT)	).
				times(	1-Math.cos( angle.doubleValue(SI.RADIAN) )	).
				plus(	incidEn.divide(incidEn)	);
		
		Amount<Dimensionless> KleinNishinaFactor = (Amount<Dimensionless>) ((ENERGY_RATIO.inverse()).pow(2)).
				times(ENERGY_RATIO.inverse().plus(ENERGY_RATIO).
						minus(Amount.valueOf(
								Math.pow(	Math.sin( angle.doubleValue(SI.RADIAN) ), 2	)
									,Unit.ONE)
								)
				).to(Unit.ONE);
		
		return KleinNishinaFactor;
	};
}
