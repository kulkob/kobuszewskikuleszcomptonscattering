package pl.pw.edu.fizyka.pojava.comptoncrosssections;

import java.sql.SQLException;

import javax.swing.SwingUtilities;

import org.jscience.physics.model.RelativisticModel;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DataStorage;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.FrameChooserLanguage;

/**
 * Main class containing static method for running program
 * 
 * @author Karolina Kulesz, Konrad Kobuszewski
 */
public class Main {
	/**
	 * Static fields for instances of DataStorage and Engine,
	 * to be visible in whole program.
	 */
	public static volatile DataStorage storage;
	public static void main(String[] args)
	{
		try {
			DBConnector db = new DBConnector();
			db.createTable();
			db.connclose();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RelativisticModel.select();//Relativistic model for computation in JScience
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				FrameChooserLanguage frameChooserLanguage = new FrameChooserLanguage();
				frameChooserLanguage.setVisible(true);
			}
		});
		
	}
}
