package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;

/**
 * ClearDBListener substitues the table with saved data in DataHolder.db with an empty table. 
 * 
 * @author Karolina Kulesz
 *
 */
public class ClearDBListener implements ActionListener {
	
	ResourceBundle messages = null;
	
	public ClearDBListener(ResourceBundle messages){
		super();
		this.messages = messages;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {
			DBConnector db = new DBConnector();
			db.createTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(null, messages.getString("MainWindow.clearDBMes"));
	}

}