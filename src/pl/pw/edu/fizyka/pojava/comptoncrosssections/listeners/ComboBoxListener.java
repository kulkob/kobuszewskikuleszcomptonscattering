package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener sets distOption, which is the field of instance of MainWindow class. 
 * distOption is used by ApplyDistributionListener.
 * 
 * @author Karolina Kulesz
 *
 */

public class ComboBoxListener implements ActionListener {
	
	MainWindow frame;
	
	public ComboBoxListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		JComboBox<String> distOption = (JComboBox<String>) e.getSource();
		frame.distOption = (String) distOption.getSelectedItem();
		System.out.println(frame.distOption);
	}
	
}
