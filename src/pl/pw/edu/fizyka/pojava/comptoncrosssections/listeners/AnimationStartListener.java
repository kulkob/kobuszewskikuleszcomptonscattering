package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener responsible starting the animation if energy distribution is selected. 
 * 
 * @author Karolina Kulesz
 *
 */
public class AnimationStartListener implements ActionListener {
	
	MainWindow frame = null;
	
	public AnimationStartListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(	(MainWindow.isStopped||MainWindow.isPaused) && MainWindow.hasDistribution	){
			frame.animation.execute();
			MainWindow.isPaused=false;
			MainWindow.isStarted=true;
			MainWindow.isStopped=false;
			frame.tabbedPane.setEnabledAt(1, false);
			frame.tabbedPane.setSelectedIndex(0);
		}
		else if (!MainWindow.hasDistribution){
			JOptionPane.showMessageDialog(null, "Choose distribution of energy first, please!");
		}
	}

}
