package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx.AnimationProviderFX;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * This listener is responsible for accelerating animation. Single button-click reduces
 * animation duration about 0,5 second.
 * 
 * @author Karolina Kulesz
 *
 */
public class AnimationAccListener implements ActionListener {
	
	MainWindow frame = null;
	
	public AnimationAccListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if( MainWindow.isStarted && (frame.duration>=1000) ){
			frame.animation.stop();
			frame.duration -= 500;
			frame.animation = new AnimationProviderFX(frame.duration,
													  frame.jfxPanel,
													  frame.tablePanel);
			frame.animation.execute();
		}
	}

}