/**
 * 
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * This Listener displays a frame with theoretical description of Compton Scattering.
 * 
 * @author Konrad Kobuszewski
 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
 */
public class TheoryDescriptionListener implements ActionListener {
	MainWindow frame=null;
	
	public TheoryDescriptionListener (MainWindow frame){
		this.frame=frame;	
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {
			Desktop.getDesktop().browse(Main.class.getResource(frame.messages.getString("TheoryDescriptionListener.ComptonDescritpion")).toURI());
		} catch (IOException | URISyntaxException e1) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e1.getMessage());
			e1.printStackTrace();
			
			JEditorPane edit = new JEditorPane();
			edit.setEditable(false);
			try {
				edit.setPage(Main.class.getResource(frame.messages.getString("TheoryDescriptionListener.htmlComptonDescritption")));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JScrollPane editorScrollPane = new JScrollPane(edit);
			editorScrollPane.setPreferredSize(new Dimension(150, 250));
			
			JFrame frameTheory = new JFrame();
			frameTheory.getContentPane().add(editorScrollPane);
			frameTheory.setSize(600,500);
			frameTheory.setVisible(true);
		}
	}
	
	
}