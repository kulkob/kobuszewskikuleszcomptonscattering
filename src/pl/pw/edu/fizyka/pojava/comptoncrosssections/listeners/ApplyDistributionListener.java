package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;
/**
 * When type of the distribution is chosen in the Combobox or Slider,
 * it is added by action listener to DataStorage.
 * 
 * @author Karolina Kulesz
 *
 */
public class ApplyDistributionListener implements ActionListener{
	
	MainWindow frame = null;
	
	public ApplyDistributionListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if( frame.distOption == null)
			return;
		if(  frame.distOption.equals( frame.messages.getString("MainWindow.targetMo") )  ){
			MainWindow.hasDistribution = true;
			Main.storage.addMoDistrib();
		}
		else if (  frame.distOption.equals( frame.messages.getString("MainWindow.targetW") )  ){
			MainWindow.hasDistribution = true;
			Main.storage.addWfDistrib();
		}
		else if (  frame.distOption.equals( frame.messages.getString("MainWindow.sourceCs") )  ){
			MainWindow.hasDistribution = true;
			Main.storage.addCsDistrib();
		}
		else if (  frame.distOption.equals( frame.messages.getString("MainWindow.sourceCo") )  ){
			MainWindow.hasDistribution = true;
			Main.storage.addCoDistrib();
		}
		else if (frame.distOption.equals("Simple Channel")){
			MainWindow.hasDistribution = true;
			Main.storage.addSimpleChannel(frame.sliderValue);
		}
		else{
			MainWindow.hasDistribution = false;
		}
	}
	
}
