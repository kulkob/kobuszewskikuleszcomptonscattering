package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx.AnimationProviderFX;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener responsible for ending the animation. 
 * It clears the current distribution of incident photons' energy.
 * 
 * @author Karolina Kulesz
 *
 */
public class AnimationStopListener implements ActionListener {
	
	MainWindow frame = null;
	
	public AnimationStopListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		MainWindow.isPaused=false;
		MainWindow.isStarted=false;
		MainWindow.isStopped=true;
		frame.tabbedPane.setEnabledAt(1, true);
		frame.animation.stop();
		frame.animation = new AnimationProviderFX(frame.duration,
												  frame.jfxPanel,
												  frame.tablePanel);
		Main.storage.clearDist();
		MainWindow.hasDistribution=false;
	}

}
