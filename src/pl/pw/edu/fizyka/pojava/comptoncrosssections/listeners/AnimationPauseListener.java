package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx.AnimationProviderFX;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener responsible for pausing the animation. It does not clear the current distribution.
 * The information about the dynamics of animation is stored in MainWindow.
 * 
 * @author Karolina Kulesz
 *
 */
public class AnimationPauseListener implements ActionListener {
	
	MainWindow frame = null;
	
	public AnimationPauseListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if( MainWindow.isStarted ){
			frame.animation.stop();
			MainWindow.isPaused = true;
			MainWindow.isStarted = false;
			MainWindow.isStopped = false;
			frame.tabbedPane.setEnabledAt(1, true);
			frame.animation = new AnimationProviderFX(frame.duration,
													  frame.jfxPanel,
													  frame.tablePanel);
			
		}
	}

}