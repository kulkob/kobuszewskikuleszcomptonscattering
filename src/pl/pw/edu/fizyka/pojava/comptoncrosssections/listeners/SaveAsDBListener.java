package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * This listener creates new instance of DBConnector (which makes new connection to DataHolder.db)
 *  and evokes function saveAsDB() from class DBConnector, 
 *  which enables saving data from DataHolder into a database specified by a user.
 * 
 * @author Karolina Kulesz
 *
 */
public class SaveAsDBListener implements ActionListener {
	
	MainWindow frame = null;
	
	public SaveAsDBListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {
			DBConnector db = new DBConnector();
			db.saveAsDB();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
