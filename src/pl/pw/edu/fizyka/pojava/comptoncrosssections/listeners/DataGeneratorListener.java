package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener turns the engine responsible for computations
 *  as well as generating and adding data to database.
 * 
 * @author Karolina Kulesz
 *
 */

public class DataGeneratorListener implements ActionListener {
	
	MainWindow frame;
	public static boolean statusDone=false;
	
	public DataGeneratorListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(MainWindow.hasDistribution){
			frame.engine.execute();
			frame.btnGenerateData.setEnabled(false);
			MainWindow.isDBCreated = true;
		}
		else{
			JOptionPane.
				showMessageDialog(null, frame.messages.getString("MainWindow.noDistribution"));
			frame.btnGenerateData.setSelected(false);
		}
	}
	
}
