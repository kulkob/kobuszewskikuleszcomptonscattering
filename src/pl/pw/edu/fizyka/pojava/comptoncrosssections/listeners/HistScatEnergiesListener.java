package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.ScatteredEnergyHistogram;

/**
 * The listener generating the window with the histogram
 * showing distribution of photons' energy after scattering.
 * 
 * @author Karolina Kulesz
 *
 */
public class HistScatEnergiesListener implements ActionListener {
	MainWindow frame = null;
	
	public HistScatEnergiesListener(MainWindow frame){
		this.frame = frame;
	}
	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(!MainWindow.showsHistogram3){
			MainWindow.showsHistogram3 = true;
			SwingUtilities.invokeLater(
				new ScatteredEnergyHistogram(frame.messages));
		}
	}

}

