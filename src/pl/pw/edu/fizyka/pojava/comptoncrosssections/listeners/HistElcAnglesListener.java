package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.AnglesElectronHistogram;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener generating the window with the histogram
 * showing electrons' scattering angle distribution.
 * 
 * @author Karolina Kulesz
 *
 */
public class HistElcAnglesListener implements ActionListener {
	MainWindow frame = null;
	
	public HistElcAnglesListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(!MainWindow.showsHistogram2){
			MainWindow.showsHistogram2 = true;
			SwingUtilities.invokeLater(
				new AnglesElectronHistogram(frame.messages));
		}
	}

}
