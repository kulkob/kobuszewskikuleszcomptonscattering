package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx.AnimationProviderFX;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener responsible for slowing the animation. Single button-click extends
 * animation duration about 0,5 second.
 * 
 * @author Karolina Kulesz
 *
 */
public class AnimationSlowListener implements ActionListener {
	
	MainWindow frame = null;
	
	public AnimationSlowListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if( MainWindow.isStarted && (frame.duration<10000) ){
			frame.animation.stop();
			frame.duration += 500;
			frame.animation = new AnimationProviderFX(frame.duration,
													  frame.jfxPanel,
													  frame.tablePanel);
			frame.animation.execute();
		}
	}

}
