package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener creates new instance of DBConnector and evokes the method saveAsTxt,
 * which rewrites database to txt file to location chosen by jFileChooser.
 * 
 * @author Karolina Kulesz
 *
 */
public class SaveAsTxtListener implements ActionListener {
	
	MainWindow frame = null;
	
	public SaveAsTxtListener(MainWindow frame){
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {
			DBConnector db = new DBConnector();
			db.saveAsTxt();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
