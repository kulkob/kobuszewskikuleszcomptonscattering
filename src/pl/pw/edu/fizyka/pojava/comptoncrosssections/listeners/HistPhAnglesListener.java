package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.AngleCountsHistogram;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * The listener generating the window with the histogram
 * showing photons' scattering angle distribution.
 * 
 * @author Karolina Kulesz
 *
 */
public class HistPhAnglesListener implements ActionListener {
	MainWindow frame = null;
	
	public HistPhAnglesListener(MainWindow frame){
		this.frame = frame;
	}
	
	public HistPhAnglesListener(){
		super();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(!MainWindow.showsHistogram1){
			MainWindow.showsHistogram1 = true;
			SwingUtilities.invokeLater(
				new AngleCountsHistogram(frame.messages));
		}
	}

}
