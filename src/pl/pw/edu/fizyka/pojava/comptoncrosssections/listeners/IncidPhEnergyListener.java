package pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.IncidPhEnergyDistribChart;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.MainWindow;

/**
 * 
 * This listiner iniciates IncidPhEnergyDistribChart in chosen language version.
 * @author Karolina Kulesz
 *
 */

public class IncidPhEnergyListener implements ActionListener {
	MainWindow frame=null;
	
	public IncidPhEnergyListener(MainWindow frame){
		this.frame=frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(!MainWindow.showsHistogram4){
			MainWindow.showsHistogram4 = true;
			SwingUtilities.invokeLater(
				new IncidPhEnergyDistribChart(frame.messages));
		}
	}

}
