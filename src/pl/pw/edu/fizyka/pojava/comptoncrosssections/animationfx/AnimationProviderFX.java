package pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx;

import java.awt.Dimension;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javafx.embed.swing.JFXPanel;
import javafx.scene.shape.Polyline;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Energy;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import org.jscience.physics.amount.Amount;
import org.jscience.physics.amount.Constants;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.computations.RandomAngle;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.computations.RandomEnergy;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.Results;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.gui.TablePanel;

/**
 * This class provides all necessary computations for animations, updates data base,
 * runs animation and refreshes table with results of animation.
 * 
 * @author Konrad Kobuszewski, Karolina Kulesz
 *
 */
public class AnimationProviderFX extends SwingWorker<Void, AnimationFX>{
	JFrame animationFrame;
	JPanel contentPane;
	JFXPanel jfxPanel;
	TablePanel table;
	DBConnector db = null;
	Timer timer = null;
	long durationMills = 2000;
	long delay = 500;
	int counter = 0;
	
	/**
	 * Constructor of class AnimationProviderFX. Show animation in a separate window.
	 * @deprecated
	 * @param duration
	 */
	public AnimationProviderFX(long duration){
		super();
		durationMills = duration;
		
		jfxPanel = new JFXPanel();
		jfxPanel.setMinimumSize(new Dimension(500, 500));
		jfxPanel.setMaximumSize(new Dimension(500, 500));
		jfxPanel.setPreferredSize(new Dimension(500,500));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		animationFrame = new JFrame("Compton scattering animation");
		animationFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		animationFrame.setBounds(200, 200, 600, 600);
		animationFrame.setContentPane(contentPane);
		contentPane.add(jfxPanel);
		animationFrame.setVisible(true);
	}
	
	/**
	 * Constructor of class AnimationProviderFX. Creates animation on given JFXPanel.
	 * 
	 * @param duration
	 */
	public AnimationProviderFX(long duration, JFXPanel jfxPanel, TablePanel table){
		super();
		this.durationMills = duration;
		this.jfxPanel = jfxPanel;
		this.table = table;
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		// TODO Auto-generated method stub
		try {
			db = new DBConnector();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					e.getMessage()+" in class AnimationProvider");
		}
		timer = new Timer();
	   	timer.scheduleAtFixedRate(
	    new TimerTask(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					counter++;
					RandomEnergy randomEnergy = new RandomEnergy();
					RandomAngle randomAngle = new RandomAngle();
					
					Amount<Energy> currentEnergy = randomEnergy.rand();
					Amount<Angle> anglePh = randomAngle.randAngle(currentEnergy);
					double angleElc = Results.countElcAngleDeg(currentEnergy, 
							anglePh)
							.doubleValue(SI.RADIAN);
					Amount<Energy> scatPhEnKeV = Results.countOutPhEnergyKeV(currentEnergy,
							anglePh).to(Results.KEV);
					
					double gamma = Results.countGamma(currentEnergy,
							Results.countOutPhEnergyKeV(currentEnergy,
							anglePh));
					
					double lengthFactor = Math.sqrt(1 - 1/Math.pow(gamma, 2));
					double wavelenght1 = Results.me.times(Constants.two_π)
							.divide(currentEnergy.times(Constants.α))
							.to(Unit.ONE).doubleValue(Unit.ONE);
					double wavelenght2 = Results.me.times(Constants.two_π)
							.divide(scatPhEnKeV.times(Constants.α))
							.to(Unit.ONE).doubleValue(Unit.ONE);
					
					//Shape for incident photon
					Polyline incidPhoton = new Polyline();
					int l1 = (int) (10*Math.log(wavelenght1));
					Double[] points1 = new Double[2*l1];
					for(int ii=0; ii < l1; ii++){
						points1[2*ii] = (double) ii+5.0;
						points1[2*ii+1] = Math.sqrt(currentEnergy.doubleValue(Results.KEV))
								*Math.sin(  Constants.π.doubleValue(Unit.ONE)*ii/l1)
								*Math.sin(9*Constants.π.doubleValue(Unit.ONE)*ii/l1)
								+jfxPanel.getHeight()/2;
					}
					incidPhoton.getPoints().addAll(points1);
					
					//Shape for scattered photon
					Polyline scatPhoton = new Polyline();
					int l2 = (int) (10*Math.log(wavelenght2));
					Double[] points2 = new Double[2*l2];
					for(int ii=0; ii < l2; ii++){
						points2[2*ii] = (double) (ii+jfxPanel.getWidth());
						points2[2*ii+1] = Math.sqrt(scatPhEnKeV.doubleValue(Results.KEV))
								*Math.sin(  Constants.π.doubleValue(Unit.ONE)*ii/l2)
								*Math.sin(9*Constants.π.doubleValue(Unit.ONE)*ii/l2)
								+jfxPanel.getHeight()/2;
					}
					scatPhoton.getPoints().addAll(points2);
					
					AnimationFX animation = new AnimationFX(
							jfxPanel,
							anglePh.doubleValue(SI.RADIAN),
							angleElc,
							lengthFactor,
							incidPhoton,
							scatPhoton,
							wavelenght1*Results.re_SI*Math.pow(10, 12),
							wavelenght2*Results.re_SI*Math.pow(10, 12),
							durationMills);
					
					Vector<Object> dataForTable = new Vector<Object>();
					dataForTable.add(AnimationFX.round(currentEnergy.doubleValue(Results.KEV),4));
					dataForTable.add(AnimationFX.round(wavelenght1*Results.re_SI*Math.pow(10, 12), 4));
					dataForTable.add(AnimationFX.round(scatPhEnKeV.doubleValue(Results.KEV),4));
					dataForTable.add(AnimationFX.round(wavelenght2*Results.re_SI*Math.pow(10, 12), 4));
					dataForTable.add(AnimationFX.round(anglePh.doubleValue(NonSI.DEGREE_ANGLE),4));
					dataForTable.add(AnimationFX.round(Math.toDegrees(angleElc),4));
					dataForTable.add(AnimationFX.round(lengthFactor, 4));
					table.addResultsRow(dataForTable);
					
					try {
						if(!(db == null))
						db.insertData(20000+counter,
									  currentEnergy,
									  anglePh);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JOptionPane.showMessageDialog(null,
								e.getMessage()+" in class AnimationProvider");
					}
					
					publish(animation);
				}
	    },
	    delay,
	    durationMills);
		
		return null;
	}
	
	@Override
	protected void done(){
		
	}
	
	@Override
	protected void process(List<AnimationFX> animations){
		for(final AnimationFX animation : animations){
			
			SwingUtilities.invokeLater(new Runnable(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					animation.initAndShow();
				}	
			});
			
		}
		
	}
	
	/**
	 * This method sets duration of animation.
	 * 
	 * @param duration - duration of animation in ms
	 */
	public void setDuration(long duration){
		this.durationMills = duration;
	}
	
	/**
	 * Cancels animation. 
	 * It is necessary to make new instance of this class to restart animation. 
	 */
	public void stop(){
		if(!(timer==null))
			timer.cancel();
	}
}