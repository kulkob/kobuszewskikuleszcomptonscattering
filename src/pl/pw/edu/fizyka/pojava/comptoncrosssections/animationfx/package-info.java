/**
 * Package provides classes displaying animationJFXPanel.
 * AnimationProviderFX.java runs computations for TablePanel.java
 */
/**
 * @author Konrad Kobuszewski, Karolina Kulesz
 *
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx;