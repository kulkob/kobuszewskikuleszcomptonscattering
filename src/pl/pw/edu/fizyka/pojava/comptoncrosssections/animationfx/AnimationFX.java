package pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.util.Duration;

import javax.measure.unit.SI;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.Results;

/**
 * This class enables providing an animation to an instance of JFXPanel created
 * in EDT. JFX Panel, when created, should be invoked in class constructor as
 * parameter. To initialize an animation it is necessary to invoke new runnable
 * in EDT by SwingUtilities.invokeLater(...);
 * 
 * @author Konrad Kobuszewski, Karolina Kulesz
 */
public class AnimationFX {
	
	volatile static JFXPanel animationPanel;
	volatile static double anglePh;
	volatile static double angleElc;
	volatile static double lengthFator;
	volatile static double wavelength1;
	volatile static double wavelength2;
	volatile static double duration = 10;
	volatile static double lateral;
	private static Polyline incidPhoton;
	private static Polyline scatPhoton;
	
/***************************************** Constructor *******************************************/
	//TODO Constructor
	/**
	 * Constructor of class AnimationFX. Creates an animation supplier.
	 * 
	 * @param jfxPanel
	 *            - JFXPanel that the animation will be shown on.
	 * @param scatPhAngle - angle that photon is scattered on.
	 * @param scatElcAngle - angle that electrons is scattered on.
	 * @param lengthFactor - relativistic ratio of electron's velocity to speed of light.
	 * A factor that length of photon's way must be multiplied to get length of electron's way
	 * (ways in the same period of time).
	 * @param incidPhoton - Polyline representing incident photon. The length of shape is relative
	 * to logarithm of photon's wavelenght (in case to fit the image). The Amplitude is relative to
	 * square root of energy (as in classical electromagnetic wave).
	 * @param scatPhoton - Polyline representing scattered photon. The length of shape is relative
	 * to logarithm of photon's wavelenght (in case to fit the image), so it's larger than in 
	 * incidPhoton. 
	 * The Amplitude is relative to square root of energy (as in classical electromagnetic wave),
	 * so it's smaller than in incidPhoton.
	 * @param wavelenght1
	 * @param wavelength2
	 * @param duration - duration of animation in milliseconds.
	 */
	public AnimationFX(	JFXPanel jfxPanel,
						double scatPhAngle,
						double scatElcAngle,
						double lengthFactor,
						Polyline incidPhoton,
						Polyline scatPhoton,
						double wavelenght1,
						double wavelength2,
						double duration) {

		AnimationFX.animationPanel = jfxPanel;
		AnimationFX.anglePh = scatPhAngle;
		AnimationFX.angleElc = scatElcAngle;
		AnimationFX.lengthFator = lengthFactor;
		AnimationFX.duration = duration;
		AnimationFX.incidPhoton = incidPhoton;
		AnimationFX.scatPhoton = scatPhoton;
		AnimationFX.wavelength1 = wavelenght1;
		AnimationFX.wavelength2 = wavelength2;
		AnimationFX.lateral = Math.min(jfxPanel.getHeight() / 2, jfxPanel.getWidth() / 2);
		
		scatPhoton.setOpacity(0.0);
	}

/******************************** Initialization of JavaFX ***************************************/
	// TODO Initialization of JavaFX
	/**
	 * Invokes the JFXPanel in FX user thread.
	 */
	public void initAndShow() {
		// This method is invoked in the EDT thread. Runs JavaFX thread.
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				initFX();
			}
		});
	}

	/**
	 * Initializes the JavaFX elements on JFXPanel and starts animation.
	 */
	public static void initFX() {
		// This method is invoked on the JavaFX thread
		final Group rootGroup = new Group();// Groups JFX elements
		final Scene scene = new Scene(rootGroup, animationPanel.getWidth(),
				animationPanel.getHeight(), Color.GHOSTWHITE);// Scene for JFX elements
		
		animationPanel.setScene(scene);
		animationPanel.setVisible(true);
		applyAnimation(rootGroup);
		
		//Displaying results
		Text lambda1 = new Text(10,20,"\u039B\u2081 = "+round(wavelength1,3)+" nm");
		Text lambda2 = new Text(10,40,"\u039B\u2082 = "+round(wavelength2,3)+" nm");
		Text theta = new Text(10, animationPanel.getHeight() - 20,
									  "\u03B8 = "+round(Math.toDegrees(anglePh),1)+" \u00B0");
		Text phi= new Text(animationPanel.getWidth() - 65, animationPanel.getHeight() - 20,
				  					  "\u03C6 = "+round(-Math.toDegrees(angleElc),1)+" \u00B0");
		Text re = new Text(animationPanel.getWidth() - 120,20,"r\u2091 = "
				+round(Results.re.doubleValue( SI.FEMTO(SI.METER) ),5)+"\u00B710\u207B\u2076 nm");
		Text vc = new Text(animationPanel.getWidth() - 70,40,"v/c = "+round(lengthFator,3));
		rootGroup.getChildren().add(lambda1);
		rootGroup.getChildren().add(lambda2);
		rootGroup.getChildren().add(theta);
		rootGroup.getChildren().add(phi);
		rootGroup.getChildren().add(vc);
		rootGroup.getChildren().add(re);
		
		//Displaying protractor
		for(int ii=0; ii < 72; ii++){
			Line protractorLine = new Line(animationPanel.getWidth()/2
					   					   +lateral*0.01*Math.cos(Math.toRadians(360*ii/72)),
					   					   animationPanel.getHeight()/2
										   -lateral*0.01*Math.sin(Math.toRadians(360*ii/72)),
										   animationPanel.getWidth()/2
										   +lateral*0.8*Math.cos(Math.toRadians(360*ii/72)),
										   animationPanel.getHeight()/2
										   -lateral*0.8*Math.sin(Math.toRadians(360*ii/72))
										);
			protractorLine.setOpacity(0.05);
			if( ii%3 == 0){
				protractorLine.setOpacity(0.1);
				Text angle = new Text(animationPanel.getWidth()/2
									  +lateral*0.9*Math.cos(Math.toRadians(360*ii/72))
									  -0.01*animationPanel.getWidth(),
						   			  animationPanel.getHeight()/2
						   			  -lateral*0.87*Math.sin(Math.toRadians(360*ii/72))
						   			  +0.01*animationPanel.getHeight(),
						   			  360*ii/72+"\u00B0"
									 );
				angle.setOpacity(0.3);
				rootGroup.getChildren().add(angle);
			}
			rootGroup.getChildren().add(protractorLine);
		}
		
		for(int ii=0; ii<3; ii++){
			Circle circle = new Circle( animationPanel.getWidth()/2,
										animationPanel.getHeight()/2,
										lateral*0.8*(ii+1)/3);
			circle.setFill(Color.ANTIQUEWHITE);
			circle.setStroke(Color.DARKGOLDENROD);
			circle.setOpacity(0.1);
			rootGroup.getChildren().add(circle);
		}
		
		animationPanel.setVisible(true);
	}

/******************************************** Paths **********************************************/
	// TODO Paths
	/**
	 * Generate Path upon which animation of incident photon will occur.
	 * 
	 * @param pathOpacity
	 *            The opacity of the path representation.
	 * @return Generated path.
	 */
	private static Path generateIncidentPhotonPath(final double pathOpacity) {
		final Path path = new Path();
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(animationPanel.getWidth() * 0.5 - lateral);
		moveTo.setY(animationPanel.getHeight() * 0.5);
		
		HLineTo lineOfIncidence = new HLineTo();
		lineOfIncidence.setX(animationPanel.getWidth() * 0.5 
				- scatPhoton.getPoints().size()/5
				- 10);
						
		path.getElements().add(moveTo);
		path.getElements().add(lineOfIncidence);
				
		path.setOpacity(pathOpacity);
		return path;
	}
	
	/**
	 * Generate Path upon which animation of scattered photon will occur.
	 * 
	 * @param pathOpacity
	 *            The opacity of the path representation.
	 * @return Generated path.
	 */
	private static Path generateScatteredPhotonPath(final double pathOpacity) {
		final Path path = new Path();
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(animationPanel.getWidth() * 0.5
					+ scatPhoton.getPoints().size()*Math.cos(anglePh)/5);
		moveTo.setY(animationPanel.getHeight() * 0.5
					- scatPhoton.getPoints().size()*Math.sin(anglePh)/5);
		
		LineTo lineOfScattering = new LineTo();
		lineOfScattering.setX(animationPanel.getWidth() * 0.5
				+ lateral * Math.cos(anglePh) );
		lineOfScattering.setY(animationPanel.getHeight() * 0.5
				- lateral * Math.sin(anglePh) );
		
		path.getElements().add(moveTo);
		path.getElements().add(lineOfScattering);
		
		path.setOpacity(pathOpacity);
		return path;
	}

	/**
	 * Generate Path upon which animation of electron will occur.
	 * 
	 * @param pathOpacity
	 *            The opacity of the path representation.
	 * @return Generated path.
	 */
	private static Path generateElectronPath(final double pathOpacity) {
		final Path path = new Path();
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(animationPanel.getWidth() * 0.5);
		moveTo.setY(animationPanel.getHeight() * 0.5);
		
		LineTo lineOfElectron = new LineTo();
		lineOfElectron.setX(animationPanel.getWidth() * 0.5
				+ lateral * lengthFator * Math.cos(angleElc));
		lineOfElectron.setY(animationPanel.getHeight() * 0.5
				+ lateral * lengthFator * Math.sin(angleElc));
		
		path.getElements().add(moveTo);
		path.getElements().add(lineOfElectron);
		
		path.setOpacity(pathOpacity);
		path.setFill(Color.DARKBLUE);
		return path;
	}

/*********************************** Generating PathTransition ***********************************/
	// TODO PathTransitions
	/**
	 * Generate the path transition.
	 * 
	 * @param shape
	 *            Shape to travel along path.
	 * @param path
	 *            Path to be traveled upon.
	 * @return PathTransition.
	 */
	private static PathTransition generatePathTransition(
			final Shape shape, final Path path, double delay, double duration) {
		final PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.millis(duration));
		pathTransition.setDelay(Duration.millis(0.0 + delay));
		pathTransition.setPath(path);
		pathTransition.setNode(shape);
		pathTransition
				.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		pathTransition.setCycleCount(1);// to repeat it over
										// Timeline.INDEFINITE
		pathTransition.setAutoReverse(false);
		return pathTransition;
	}
	
	/**
	 * Generates photon's transition.
	 * 
	 * @param wavelet shape to be animated.
	 * @param path path which the shape will be animated on.
	 * @param delay in millis
	 * @param duration in milis
	 * @return Photon's Transition on JFXPanel.
	 */
	private static PathTransition generatePhotonTransition(
			final Shape wavelet, final Path path, double delay, double duration) {
		final PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.millis(duration));
		pathTransition.setDelay(Duration.millis(0.0 + delay));
		pathTransition.setPath(path);
		pathTransition.setNode(wavelet);
		pathTransition
				.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		pathTransition.setCycleCount(1); // to repeat it over
										 // Timeline.INDEFINITE
		pathTransition.setAutoReverse(false);
		
		return pathTransition;
	}

/****************************************** Animation ********************************************/
	//TODO Animation
	/**
	 * Apply animation, the subject of this class.
	 * 
	 * @param group
	 *            Group created on JFXPanel to which animation is applied.
	 */
	private static void applyAnimation(final Group group) {
		//Components to be animated
		final Circle electron = new Circle(animationPanel.getWidth() * 0.5,
				animationPanel.getHeight() * 0.5, 10);
		electron.setFill(Color.BLUE);
		electron.setOpacity(1.0);
		
		Arc arcTh = new Arc();
		arcTh.setCenterX(animationPanel.getWidth()/2);
		arcTh.setCenterY(animationPanel.getHeight()/2);
		arcTh.setRadiusX(50.0f);
		arcTh.setRadiusY(50.0f);
		arcTh.setStartAngle(0);
		arcTh.setLength(Math.toDegrees(anglePh));
		arcTh.setType(ArcType.ROUND);
		arcTh.setFill(Color.TRANSPARENT);
		arcTh.setStroke(Color.GREY);
		arcTh.setOpacity(0.0);
		
		Arc arcPhi = new Arc();
		arcPhi.setCenterX(animationPanel.getWidth()/2);
		arcPhi.setCenterY(animationPanel.getHeight()/2);
		arcPhi.setRadiusX(50.0f);
		arcPhi.setRadiusY(50.0f);
		arcPhi.setStartAngle(0);
		arcPhi.setLength(Math.toDegrees(-angleElc));
		arcPhi.setType(ArcType.ROUND);
		arcPhi.setFill(Color.TRANSPARENT);
		arcPhi.setStroke(Color.GREY);
		arcPhi.setOpacity(0.0);
				
		group.getChildren().add(arcTh);
		group.getChildren().add(arcPhi);
		group.getChildren().add(incidPhoton);
		group.getChildren().add(scatPhoton);
		group.getChildren().add(electron);
		
		
		//Declaration of paths of animation's components
		final Path incidPhotonPath = generateIncidentPhotonPath(0.0);
		final Path scatPhotonPath = generateScatteredPhotonPath(0.0);
		final Path electronPath = generateElectronPath(0.0);
		
		final Line incidPhLine = new Line(  animationPanel.getWidth()/2 - lateral,
											animationPanel.getHeight()/2,
											animationPanel.getWidth()/2,
											animationPanel.getHeight()/2);
		incidPhLine.setOpacity(0.0);
		incidPhLine.setFill(Color.YELLOW);
		final Line scatPhLine = new Line(  animationPanel.getWidth()/2,
				animationPanel.getHeight()/2,
				animationPanel.getWidth() * 0.5 + lateral * Math.cos(anglePh),
				animationPanel.getHeight() * 0.5 - lateral * Math.sin(anglePh) );
		scatPhLine.setOpacity(0.0);
		scatPhLine.setFill(Color.YELLOW);
				
		group.getChildren().add(incidPhotonPath);
		group.getChildren().add(scatPhotonPath);
		group.getChildren().add(electronPath);
		group.getChildren().add(incidPhLine);
		group.getChildren().add(scatPhLine);
		
		//Path Transitions
		final PathTransition incidPhotonTransition = generatePhotonTransition(
				incidPhoton, incidPhotonPath, 0, duration / 2);
		final PathTransition scatPhotonTransition = generatePhotonTransition(
				scatPhoton, scatPhotonPath, duration / 2, duration / 2);
		final PathTransition electronTransition = generatePathTransition(
				electron, electronPath, duration / 2, duration / 2);
		
		//Fade Transitions
		final FadeTransition incidPhotonFade = new FadeTransition(
				Duration.millis(350), incidPhoton);
		incidPhotonFade.setFromValue(1.0);
		incidPhotonFade.setToValue(0.0);
		incidPhotonFade.setDelay(Duration.millis(duration/2-100));
		incidPhotonFade.setCycleCount(1);
		
		final FadeTransition scatPhotonFade1 = new FadeTransition(
				Duration.millis(100), scatPhoton);
		scatPhotonFade1.setFromValue(0.0);
		scatPhotonFade1.setToValue(1.0);
		scatPhotonFade1.setDelay(Duration.millis(duration/2));
		scatPhotonFade1.setCycleCount(1);
		final FadeTransition scatPhotonFade2 = new FadeTransition(
				Duration.millis(500), scatPhoton);
		scatPhotonFade2.setFromValue(1.0);
		scatPhotonFade2.setToValue(0.0);
		scatPhotonFade2.setDelay(Duration.millis(duration-500));
		scatPhotonFade2.setCycleCount(1);
		
		final FadeTransition electronFade = new FadeTransition(
				Duration.millis(200), electron);
		electronFade.setFromValue(1.0);
		electronFade.setToValue(0.0);
		electronFade.setCycleCount(1);
		
		final FadeTransition arcThFade = new FadeTransition(
				Duration.millis(200),arcTh);
		arcThFade.setFromValue(0.0);
		arcThFade.setToValue(0.3);
		arcThFade.setCycleCount(1);
		arcThFade.setDelay(Duration.millis(duration - 500));		
		final FadeTransition arcPhiFade = new FadeTransition(
				Duration.millis(200),arcPhi);
		arcPhiFade.setFromValue(0.0);
		arcPhiFade.setToValue(0.3);
		arcPhiFade.setCycleCount(1);
		arcPhiFade.setDelay(Duration.millis(duration - 500));
		
		final FadeTransition pathInPhFade = new FadeTransition(
				Duration.millis(200),incidPhLine);
		pathInPhFade.setFromValue(0.0);
		pathInPhFade.setToValue(0.8);
		pathInPhFade.setCycleCount(1);
		pathInPhFade.setDelay(Duration.millis(duration - 500));
		final FadeTransition pathScatPhFade = new FadeTransition(
				Duration.millis(200),scatPhLine);
		pathScatPhFade.setFromValue(0.0);
		pathScatPhFade.setToValue(0.8);
		pathScatPhFade.setCycleCount(1);
		pathScatPhFade.setDelay(Duration.millis(duration - 500));
		final FadeTransition pathElcFade = new FadeTransition(
				Duration.millis(200),electronPath);
		pathElcFade.setFromValue(0.0);
		pathElcFade.setToValue(0.8);
		pathElcFade.setCycleCount(1);
		pathElcFade.setDelay(Duration.millis(duration - 500));
		
		//Controls of sequence of animation
		final ParallelTransition parallelTransition1 = new ParallelTransition();
		parallelTransition1.getChildren().addAll(
				incidPhotonTransition,
				incidPhotonFade,scatPhotonTransition,
				electronTransition,
				scatPhotonFade1,
				scatPhotonFade2,
				arcThFade,
				arcPhiFade,
				pathInPhFade,
				pathScatPhFade,
				pathElcFade);
		parallelTransition1.setCycleCount(1);
		
		//Final animation
		SequentialTransition animation = new SequentialTransition();
		animation.getChildren().addAll(
				parallelTransition1,
				//parallelTransition2,
				electronFade);
		animation.setCycleCount(1);
		
		animation.play();
		
	}
	
	/**
	 * Function for rounding doubles with given precison
	 * 
	 * @param value
	 * @param places
	 * @return
	 */
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
}