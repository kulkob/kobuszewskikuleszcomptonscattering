package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

/**
 * 
 * This class contains JPanel for language choosing with PL and EN buttons.
 * Necessary for language choosing listeners are implemented here.
 * 
 * @author Karolina Kulesz
 * 
 */
public class FrameChooserLanguage extends JFrame 
{

	private static final long serialVersionUID = -7530307730961385503L;
	public FrameChooserLanguage()
	{
		super("Wybierz j\u0119zyk / Choose language");
		setSize(300,100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//choosing language panel
		JPanel panelChooserLanguage = new JPanel();
		add(panelChooserLanguage);
		panelChooserLanguage.setLayout(new GridLayout(1, 2));
		
		//Buttons PL|EN - choosing language
		JButton chooserPolishLanguageButton = new JButton("PL");
		panelChooserLanguage.add(chooserPolishLanguageButton);
		chooserPolishLanguageButton.addActionListener(new ChooserPolishLanguageButtonListener(this));
		
		JButton chooserEnglishLanguageButton = new JButton("EN");
		panelChooserLanguage.add(chooserEnglishLanguageButton);
		chooserEnglishLanguageButton.addActionListener(new ChooserEnglishLanguageButtonListener(this));
		
		//Setting default button
		JRootPane rootPane = SwingUtilities.getRootPane(chooserPolishLanguageButton); 
		rootPane.setDefaultButton(chooserPolishLanguageButton);
	}	
}
