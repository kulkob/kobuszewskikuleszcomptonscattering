package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.ui.RefineryUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;

/**
 * Frame with BarChart representing distribution of energy of incident photons. 
 * Written using example from http://www.java2s.com/Code/Java/Chart/JFreeChartBarChartDemo.html
 * 
 * @author Konrad Kobuszewski
 *
 */
public class IncidPhEnergyDistribChart extends JFrame implements Runnable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4240023706766774612L;
	static ResourceBundle messages;

	/**
     * Creates new frame with BarChart representing distribution of energy of incident photons.
     *
     * @param title  the frame title.
     */
    public IncidPhEnergyDistribChart(ResourceBundle message) {
        super(message.getString("IncidPhEnergyDistribChart.title"));
        IncidPhEnergyDistribChart.messages=message;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        try{
        	DBConnector db = new DBConnector();
        	final CategoryDataset dataset = db.getIncidentPhEnergies();
        	        	
        	final JFreeChart chart = createChart(dataset);
            final ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new java.awt.Dimension(800, 575));
            setContentPane(chartPanel);
            
        } catch(SQLException e){
        	JOptionPane.showMessageDialog(null, e.getMessage());
        	e.printStackTrace();
        }

        addWindowListener(exitListener);
    }
    
    /**
     * Creates a sample chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return The chart.
     */
    private JFreeChart createChart(final CategoryDataset dataset) {
        
        final JFreeChart chart = ChartFactory.createBarChart(
        messages.getString("IncidPhEnergyDistribChart.title"), // chart title
        messages.getString("IncidPhEnergyDistribChart.axisX"), // domain axis label
        messages.getString("IncidPhEnergyDistribChart.axisY"), // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // orientation
            true,                     // include legend
            true,                     // tooltips
            false                     // URLs?
        );
        
        chart.setBackgroundPaint(Color.white);
        
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        
        final GradientPaint gp0 = new GradientPaint(
            0.0f, 0.0f, Color.blue, 
            0.0f, 0.0f, Color.lightGray
        );
        final GradientPaint gp1 = new GradientPaint(
            0.0f, 0.0f, Color.green, 
            0.0f, 0.0f, Color.lightGray
        );
        final GradientPaint gp2 = new GradientPaint(
            0.0f, 0.0f, Color.red, 
            0.0f, 0.0f, Color.lightGray
        );
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        renderer.setSeriesPaint(2, gp2);

        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );
                
        return chart;
    }
    
    private WindowListener exitListener = new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            MainWindow.showsHistogram4=false;
        }
    };
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		IncidPhEnergyDistribChart chart = this;
		chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
		setVisible(true);
	}
	
}