package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.embed.swing.JFXPanel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.animationfx.AnimationProviderFX;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.computations.Engine;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.AnimationAccListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.AnimationPauseListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.AnimationSlowListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.AnimationStartListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.AnimationStopListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.ApplyDistributionListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.ClearDBListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.ComboBoxListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.DataGeneratorListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.HistElcAnglesListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.HistPhAnglesListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.HistScatEnergiesListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.IncidPhEnergyListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.SaveAsDBListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.SaveAsTxtListener;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.listeners.TheoryDescriptionListener;

/**
 * Provides application main frame. Build with WindowBuilder.
 * 
 * @author Karolina Kulesz
 * 
 * 
 */
public class MainWindow extends JFrame implements Runnable{
	
	private static final long serialVersionUID = 1L;

	private MainWindow wholeFrame;
	public volatile ResourceBundle messages;
	
	private JPanel contentPane;
	public TablePanel tablePanel;
	public volatile JFXPanel jfxPanel;
	
	public long duration = 2000;//Duration of animation. Can be used for varying speed of animation
	public volatile AnimationProviderFX animation;
	public Engine engine;
	
	protected JComboBox<String> xrayComboBox;
	protected JComboBox<String> sourceComboBox;
	protected JButton applyTarget;
	protected JButton applySource;
	protected JButton applyChannel;
	protected JSlider slider;
	public double sliderValue=20.0;
	public JTabbedPane tabbedPane;
	public JToggleButton btnGenerateData;
	
	public String distOption = null;
	
	//Flags
	public static volatile boolean hasDistribution = false;
	public static volatile boolean isDBCreated = false;
	public static volatile boolean isStarted = false;
	public static volatile boolean isPaused = false;
	public static volatile boolean isStopped = false;
	public static volatile boolean simpleChannel = false;
	public static volatile boolean sourceDist = false;
	public static volatile boolean xrayTarget = false;
	public static volatile boolean showsHistogram1 = false;
	public static volatile boolean showsHistogram2 = false;
	public static volatile boolean showsHistogram3 = false;
	public static volatile boolean showsHistogram4 = false;
	
/**************************************************************************************************
											  GUI
**************************************************************************************************/	
	/**
	 * Creates the frame.
	 * @param ResourceBundle messages 
	 * - implements translation of frame's contents in chosen language
	 */
	public MainWindow(ResourceBundle mes) {
		super(mes.getString("MainWindow.title"));
		URL iconURL = Main.class.getResource("resources/JFrameIcon.png");
		// iconURL is null when not found
		ImageIcon icon = new ImageIcon(iconURL);
		if(icon!=null){
			setIconImage(icon.getImage());
		}
			

		
		wholeFrame = this;
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 30, 870, 670);
		setMinimumSize(new Dimension(870, 670));
		messages = mes;
		isStopped = true;
		
/******************************************** MENU ***********************************************/
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu(mes.getString("MainWindow.fileMenu"));
		menuBar.add(fileMenu);

		JMenuItem saveAsDB = new JMenuItem(mes.getString("MainWindow.saveAsDB"));
		saveAsDB.addActionListener(new SaveAsDBListener(this));
		fileMenu.add(saveAsDB);
		
		JMenuItem saveAsTxt = new JMenuItem(mes.getString("MainWindow.saveAsTxt"));
		saveAsTxt.addActionListener(new SaveAsTxtListener(this));
		fileMenu.add(saveAsTxt);
		
		JMenu dataMenu = new JMenu(mes.getString("MainWindow.dataMenu"));
		menuBar.add(dataMenu);
				
		JMenuItem clearDB = new JMenuItem(mes.getString("MainWindow.clearDB"));
		clearDB.addActionListener(new ClearDBListener(messages));
		dataMenu.add(clearDB);
		
		JMenu histogramMenu = new JMenu(mes.getString("MainWindow.histogramsMenu"));
		menuBar.add(histogramMenu);
		
		JMenuItem phAngles = new JMenuItem(mes.getString("MainWindow.phAngles"));
		phAngles.addActionListener(new HistPhAnglesListener(this));
		histogramMenu.add(phAngles);
		
		JMenuItem elcAngles = new JMenuItem(mes.getString("MainWindow.elcAngles"));
		elcAngles.addActionListener(new HistElcAnglesListener(this));		
		histogramMenu.add(elcAngles);
		
		JMenuItem scatEnergies = new JMenuItem(mes.getString("MainWindow.scatEnergies"));
		scatEnergies.addActionListener(new HistScatEnergiesListener(this));
		histogramMenu.add(scatEnergies);
		
		JMenuItem phEnergy = new JMenuItem(mes.getString("MainWindow.phEnergy"));
		phEnergy.addActionListener(new IncidPhEnergyListener(this));
		histogramMenu.add(phEnergy);
		
		
		JMenu programMenu = new JMenu(mes.getString("MainWindow.aboutProgramMenu"));
		menuBar.add(programMenu);
		
		JMenuItem authors= new JMenuItem(mes.getString("MainWindow.authors"));
		authors.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				JOptionPane.showMessageDialog(null,
					    "Karolina Kulesz | Konrad Kobuszewski.",
					    "Autorzy/Authors",
					    JOptionPane.PLAIN_MESSAGE);
				
			}
		} );
		programMenu.add(authors);
		
		JMenuItem theory = new JMenuItem(mes.getString("MainWindow.theory"));
		theory.addActionListener(new TheoryDescriptionListener(this));
		programMenu.add(theory);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
/********************************** Energy Settings **********************************************/
		
		JPanel energySettingsPanel = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.NORTH, energySettingsPanel, 5,
				SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, energySettingsPanel, -300,
				SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, energySettingsPanel, 0,
				SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, energySettingsPanel, 00,
				SpringLayout.EAST, contentPane);
		contentPane.add(energySettingsPanel);
		
		JPanel customerChooseCasePanel = new JPanel();
		customerChooseCasePanel.setBorder(UIManager.getBorder("InternalFrame.border"));
		
		JPanel sourceCasePanel = new JPanel();
		sourceCasePanel.setBorder(UIManager.getBorder("InternalFrame.border"));
		
		JPanel xrayCasePanel = new JPanel();
		xrayCasePanel.setBorder(UIManager.getBorder("InternalFrame.border"));
		
		JLabel labelEnSettings = new JLabel(mes.getString("MainWindow.energySettingsPanel"));
		
		JPanel dataGenPanel = new JPanel();
		dataGenPanel.setBorder(UIManager.getBorder("InternalFrame.border"));
		GroupLayout gl_energySettingsPanel = new GroupLayout(energySettingsPanel);
		gl_energySettingsPanel.setHorizontalGroup(
			gl_energySettingsPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_energySettingsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_energySettingsPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(dataGenPanel, Alignment.TRAILING,
								GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
						.addComponent(xrayCasePanel,
								GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
						.addComponent(sourceCasePanel, Alignment.TRAILING,
								GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
						.addComponent(customerChooseCasePanel,
								GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
						.addComponent(labelEnSettings))
					.addContainerGap())
		);
		gl_energySettingsPanel.setVerticalGroup(
			gl_energySettingsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_energySettingsPanel.createSequentialGroup()
					.addComponent(labelEnSettings)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(customerChooseCasePanel, GroupLayout.PREFERRED_SIZE,
							GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(sourceCasePanel, GroupLayout.PREFERRED_SIZE,
							GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(xrayCasePanel, GroupLayout.PREFERRED_SIZE,
							GroupLayout.DEFAULT_SIZE ,GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
					.addComponent(dataGenPanel, GroupLayout.PREFERRED_SIZE, 55,
							GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		dataGenPanel.setLayout(new BorderLayout(0, 0));
		
		btnGenerateData = new JToggleButton(mes.getString("MainWindow.generateData"));
		btnGenerateData.addActionListener(new DataGeneratorListener(this));
		btnGenerateData.setActionCommand("disable data");
		btnGenerateData.setToolTipText(mes.getString("MainWindow.tipGenerateData"));
		dataGenPanel.add(btnGenerateData);
				
		//xray group
		xrayCasePanel.setLayout(new MigLayout("", "[129px][grow]", "[23px][][]"));
		
		applyTarget = new JButton(mes.getString("MainWindow.applyButton"));
		final JCheckBox checkBoxxrayCase = 
				new JCheckBox(mes.getString("MainWindow.xraySource"));
		checkBoxxrayCase.addActionListener(new ActionListener(){
			ComboBoxListener xrayListener = new ComboBoxListener(wholeFrame);
			ApplyDistributionListener targetApplier = new ApplyDistributionListener(wholeFrame);
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(checkBoxxrayCase.isSelected()){
					wholeFrame.distOption = messages.getString("MainWindow.targetMo");
					xrayTarget = true;
					xrayComboBox.addActionListener(xrayListener);
					applyTarget.addActionListener(targetApplier);
				}
				else{
					if(xrayTarget){
						wholeFrame.distOption = "";
						xrayComboBox.removeActionListener(xrayListener);
						applyTarget.removeActionListener(targetApplier);
					}
					xrayTarget = false;
				}
			}
		});
		xrayCasePanel.add(checkBoxxrayCase, "cell 0 0,alignx left,aligny top");
		
		String[] xrayOptions = { mes.getString("MainWindow.targetMo"),
									mes.getString("MainWindow.targetW")};
		xrayComboBox = new JComboBox<String>(xrayOptions);
		xrayCasePanel.add(xrayComboBox, "cell 0 1,growx");
		
		xrayCasePanel.add(applyTarget, "cell 0 2 2 1,alignx center,aligny center");
		applyTarget.setToolTipText(mes.getString("MainWindow.tipApplyButton"));
		
		//Source group
		sourceCasePanel.setLayout(new MigLayout("", "[129px][117px]", "[23px][][]"));
		
		applySource = new JButton(mes.getString("MainWindow.applyButton"));
		final JCheckBox checkBoxSourceCase =
				new JCheckBox(mes.getString("MainWindow.radioactiveSource"));
		checkBoxSourceCase.addActionListener(new ActionListener(){
			ComboBoxListener sourceListener = new ComboBoxListener(wholeFrame);
			ApplyDistributionListener sourceApplier = new ApplyDistributionListener(wholeFrame);
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub				
				if(checkBoxSourceCase.isSelected()){
					wholeFrame.distOption = messages.getString("MainWindow.sourceCs");
					sourceDist = true;
					sourceComboBox.addActionListener(sourceListener);
					applySource.addActionListener(sourceApplier);
				}
				else{
					if(sourceDist){
						wholeFrame.distOption = "";
						sourceComboBox.removeActionListener(sourceListener);
						applySource.removeActionListener(sourceApplier);
					}
					sourceDist = false;
				}
			}
		});
		sourceCasePanel.add(checkBoxSourceCase, "cell 0 0,alignx left,aligny top");
		
		String[] sourceOptions = {mes.getString("MainWindow.sourceCs"),
								  mes.getString("MainWindow.sourceCo")};
		sourceComboBox = new JComboBox<String>(sourceOptions);
		sourceCasePanel.add(sourceComboBox, "cell 0 1,growx,aligny center");
		
		sourceCasePanel.add(applySource, "cell 0 2 2 1,alignx center,aligny center");
		customerChooseCasePanel.setLayout(new MigLayout("", "[][300px][]", "[23px][33px][]"));
		applySource.setToolTipText(mes.getString("MainWindow.tipApplyButton"));
		
		//Simple channel group
		applyChannel = new JButton(mes.getString("MainWindow.applyButton"));
		final JCheckBox checkBoxCustomerCase =
				new JCheckBox(mes.getString("MainWindow.energyMagnitude"));
		checkBoxCustomerCase.addActionListener(new ActionListener(){
			ApplyDistributionListener channelApplier = new ApplyDistributionListener(wholeFrame);
			
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				// TODO Auto-generated method stub
				if(checkBoxCustomerCase.isSelected()){
					distOption = "Simple Channel";
					simpleChannel = true;
					applyChannel.addActionListener(channelApplier);
				}
				else{
					distOption = null;
					simpleChannel = false;
					applyChannel.removeActionListener(channelApplier);
				}
			}
		});
		customerChooseCasePanel.add(checkBoxCustomerCase, "cell 0 0,alignx left,aligny top");
		
		slider = new JSlider(JSlider.HORIZONTAL,
				0,		//MinValue (keV)
				2000,	//MaxValue (keV)
				20);		//InitialValue (keV)
		slider.setBorder(UIManager.getBorder("SplitPane.border"));
		slider.setToolTipText("opis\r\n");
		slider.setMinorTickSpacing(50);
		slider.setMajorTickSpacing(500);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.addChangeListener(new ChangeListener(){
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				// TODO Auto-generated method stub
				if (!slider.getValueIsAdjusting()) {
					if(slider.getValue() != 0)
						sliderValue = (double) slider.getValue();
		            else
		            	JOptionPane.showMessageDialog(null, "O.0 keV is non physical energy!");
		            System.out.println(sliderValue);
		        } 
			}
			
		});
		customerChooseCasePanel.add(slider, "cell 0 1 2 1,growx,aligny top");
		
		customerChooseCasePanel.add(applyChannel, "cell 0 2 2 1,alignx center,aligny center");
		applyChannel.setToolTipText(mes.getString("MainWindow.tipApplyButton"));
		energySettingsPanel.setLayout(gl_energySettingsPanel);
		
/********************************** Animation control ********************************************/
		
		JPanel panel = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.NORTH, panel, -37,
				SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panel, 10,
				SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panel, 0,
				SpringLayout.SOUTH, energySettingsPanel);
		sl_contentPane.putConstraint(SpringLayout.EAST, panel, -6,
				SpringLayout.WEST, energySettingsPanel);
		contentPane.add(panel);
		panel.setLayout(new MigLayout("", "[57px][][][][][][]", "[23px]"));
		
		JButton startAnimationButton = new JButton(mes.getString("MainWindow.startButton"));
		startAnimationButton.addActionListener(new AnimationStartListener(this));
		startAnimationButton.setToolTipText(messages.getString("MainWindow.tipAnimStart"));
		panel.add(startAnimationButton, "cell 1 0,alignx center,aligny center");
		
		JButton slowAnimationButton = new JButton(mes.getString("MainWindow.slowdownButton"));
		slowAnimationButton.addActionListener(new AnimationSlowListener(this));
		slowAnimationButton.setToolTipText(messages.getString("MainWindow.tipAnimSlow"));
		panel.add(slowAnimationButton, "cell 2 0,alignx center,aligny center");
		
		JButton accelerateAnimationButton = new JButton(mes.getString("MainWindow.speedupButton"));
		accelerateAnimationButton.addActionListener(new AnimationAccListener(this));
		accelerateAnimationButton.setToolTipText(messages.getString("MainWindow.tipAnimAcc"));
		panel.add(accelerateAnimationButton, "cell 3 0,alignx center,aligny center");
		
		JButton pauseAnimationButton = new JButton(mes.getString("MainWindow.pauseButton"));
		pauseAnimationButton.addActionListener(new AnimationPauseListener(this));
		pauseAnimationButton.setToolTipText(messages.getString("MainWindow.tipAnimPause"));		
		panel.add(pauseAnimationButton, "cell 4 0,alignx center,aligny center");
		
		JButton stopAnimationButton = new JButton(mes.getString("MainWindow.stopButton"));
		stopAnimationButton.addActionListener(new AnimationStopListener(this));
		stopAnimationButton.setToolTipText(messages.getString("MainWindow.tipAnimStop"));		
		panel.add(stopAnimationButton, "cell 5 0,alignx center,aligny center");
		
		
/******************************************** TabbedPane *****************************************/
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tabbedPane, 5,
				SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, tabbedPane, -11,
				SpringLayout.NORTH, panel);
		sl_contentPane.putConstraint(SpringLayout.EAST, tabbedPane, -6,
				SpringLayout.WEST, energySettingsPanel);
		sl_contentPane.putConstraint(SpringLayout.WEST, tabbedPane, 10,
				SpringLayout.WEST, contentPane);
		contentPane.add(tabbedPane);
		
		jfxPanel = new JFXPanel();
		jfxPanel.setBackground(Color.WHITE);
		jfxPanel.setMinimumSize(new Dimension(500, 500));
		jfxPanel.setToolTipText(messages.getString("MainWindow.tipAnimPanel"));
		tabbedPane.addTab(mes.getString("MainWindow.animation"), null, jfxPanel, null);
				
		//Table with results of animation
		tablePanel = new TablePanel(messages);
		tabbedPane.addTab(mes.getString("MainWindow.tables"), null, tablePanel, null);
		tablePanel.setLayout(new GridLayout(1,0));
		tablePanel.setToolTipText(messages.getString("MainWindow.tipTable"));

		animation = new AnimationProviderFX(2500, this.jfxPanel, tablePanel);
		engine = new Engine(this);
		
/************************************ WindowListener *********************************************/
		addWindowListener(new WindowListener(){

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				// TODO Auto-generated method stub
				int ans = JOptionPane.showConfirmDialog(
						wholeFrame, 
						messages.getString("MainWindow.CloseDesription"),
						messages.getString("MainWindow.CloseTitle"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if(ans == JOptionPane.YES_OPTION){
					if(animation != null)
						animation.stop();
					if(engine != null)
						engine.stop();
					wholeFrame.dispose();
					try {
						//Clearing DataHolder.h2.db
						DBConnector db = new DBConnector();
						db.createTable();
						db.connclose();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, e.getMessage());
						e.printStackTrace();
					}
					System.exit(0);
				}
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
/******************************************* GUI Components' end *********************************/	
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		setVisible(true);
	}
}
