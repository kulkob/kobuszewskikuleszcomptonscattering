/**
 * The package contains elements of GUI, all JFrames existing in the program as well as two 'language' listener classes.
 */
/**
 * @author Konrad Kobuszewski, Karolina Kulesz
 *
 */
package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;