package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.ui.RefineryUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DBConnector;

/**
 * This class creates JFrame with histogram showing counts of energies of scattered photons
 * in 72 bins from 0 to 360 degrees.
 * 
 * 
 * @author Konrad Kobuszewski
 */
public class ScatteredEnergyHistogram extends JFrame implements Runnable{

	private static final long serialVersionUID = -4650101241605298645L;
	
	double[] scatteringAngleCounts;
	static volatile HistogramDataset dataSet;
	static ResourceBundle messages;
	
	/**
	 * @deprecated
	 * @param title
	 * @param externalCounts
	 */
	public ScatteredEnergyHistogram(String title, double[] externalCounts) {
        super(title);
        scatteringAngleCounts=externalCounts;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dataSet = new HistogramDataset();
        dataSet.addSeries("Ilosc zliczen", externalCounts, 72);
        
        JPanel chartPanel = createPanel(dataSet);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 300));
        setContentPane(chartPanel);
        addWindowListener(exitListener);
	}
	
	/**
	 * Constructor of this class.
	 * 
	 * @param title Title of histogram
	 */
	public ScatteredEnergyHistogram(ResourceBundle message) {
        super(message.getString("ScatteredEnergyHistogram.titleH"));
        ScatteredEnergyHistogram.messages=message;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        try {
			DBConnector db = new DBConnector();
			dataSet = db.getScatPhEnergies();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        JPanel chartPanel = createPanel(dataSet);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 575));
        setContentPane(chartPanel);
        addWindowListener(exitListener);
	}
	
    private static JFreeChart plotHistogram(IntervalXYDataset dataset) {
        JFreeChart chart = ChartFactory.createHistogram(
        		messages.getString("ScatteredEnergyHistogram.title"),//Title
        		messages.getString("ScatteredEnergyHistogram.axisX"),//x axis description
        		messages.getString("ScatteredEnergyHistogram.axisY"),//y axis description
                dataset,
                PlotOrientation.VERTICAL,
                true,//show
                true,//toolTips
                false);//urls
        
        XYPlot plot = (XYPlot) chart.getPlot();        
        
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        
        return chart;
    }
    
    /**
     * Returns JPanel with chart showing histogram of energies of scattered photons.
     * 
     * @param dataSet HistogramDataset to be shown on chart
     * @return JPanel with chart
     */
    public static JPanel createPanel(HistogramDataset dataSet) {
        JFreeChart chart = plotHistogram(dataSet);
        return new ChartPanel(chart);
    }
    
    private WindowListener exitListener = new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            MainWindow.showsHistogram3=false;
        }
    };
    
    /**
     * Enables running as runnable in another thread.
     */
    @Override
	public void run() {
		// TODO Auto-generated method stub
		ScatteredEnergyHistogram histogram = this;
        histogram.pack();
        RefineryUtilities.centerFrameOnScreen(histogram);
        histogram.setVisible(true);
	}

}