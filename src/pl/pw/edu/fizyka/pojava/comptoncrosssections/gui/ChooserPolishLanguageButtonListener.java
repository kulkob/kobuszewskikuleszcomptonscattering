package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DataStorage;

/**
 * @author Karolina Kulesz
 */
public class ChooserPolishLanguageButtonListener implements ActionListener
{
	FrameChooserLanguage frameChooserLanguage;
	
	public ChooserPolishLanguageButtonListener (FrameChooserLanguage frameChooserLanguage)
	{
		this.frameChooserLanguage = frameChooserLanguage;
	}
	@Override
	public void actionPerformed (ActionEvent e) 
	{
		// TODO Auto-generated method stub
		frameChooserLanguage.dispose();
		ResourceBundle messages = null;
		Locale currentLocale = new Locale(new String("pl"), new String("PL"));
		messages = ResourceBundle.getBundle("pl.pw.edu.fizyka.pojava.comptoncrosssections.resources.translation", currentLocale);
		Main.storage = new DataStorage(messages);
		MainWindow projectFrame = new MainWindow(messages);
		SwingUtilities.invokeLater(projectFrame);
	};
}
