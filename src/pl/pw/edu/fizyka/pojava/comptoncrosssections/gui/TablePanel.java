package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * This class is responsible for launching table in separate tab of JFrame ,,Main Window"
 * 
 * @author Karolina Kulesz
 */
public class TablePanel extends JPanel implements TableModelListener {
	public JTable table;
	DefaultTableModel tableModel;
	Vector<String> columnNames;
	public Vector<Vector<Object>> data = null;
	private static final long serialVersionUID = 1L;
	private boolean DEBUG = false;
    
    public TablePanel(ResourceBundle message) {
        columnNames = new Vector<String>();
        columnNames.add(message.getString("TablePanel.energyIncPh"));
        columnNames.add(message.getString("TablePanel.wavelengthIncPh"));
        columnNames.add(message.getString("TablePanel.energyScaPh"));
        columnNames.add(message.getString("TablePanel.wavelengthScaPh"));
        columnNames.add(message.getString("TablePanel.AngleScaPh"));
        columnNames.add(message.getString("TablePanel.AngleScaEl"));
        columnNames.add(message.getString("TablePanel.VCRatio"));
        
        data = new Vector<Vector<Object>>();
        
        tableModel = new DefaultTableModel();
        tableModel.setDataVector(data, columnNames);
        table = new JTable(tableModel);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setPreferredScrollableViewportSize(new Dimension(200, 70));
        table.setAutoscrolls(true);
        table.setEnabled(false);;
        table.setFillsViewportHeight(true);
    	table.setShowGrid(true);
    	table.setVisible(true);
    	
        if (DEBUG) {
            table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                }
            });
        }
        
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        
        //Add the scroll pane to this panel.
        add(scrollPane);
    }
    
    public void addResultsRow(Vector<Object> dataForTable){
    	data.add(dataForTable);
    	tableModel.fireTableRowsInserted(0, 0);
    }
    
    /***niewykorzystane metody***/
    public int getColumnCount() {
        return columnNames.size();
    }

    public int getRowCount() {
        return data.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    public Object getValueAt(int row, int col) {
        return data.get(row).get(col);
    }
 
	@Override
	public void tableChanged(TableModelEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}