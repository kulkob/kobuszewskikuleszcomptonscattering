package pl.pw.edu.fizyka.pojava.comptoncrosssections.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
//import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.SwingUtilities;

import pl.pw.edu.fizyka.pojava.comptoncrosssections.Main;
import pl.pw.edu.fizyka.pojava.comptoncrosssections.datautil.DataStorage;


/**
 * 
 * @author Karolina Kulesz
 */
public class ChooserEnglishLanguageButtonListener implements ActionListener
{
	FrameChooserLanguage frameChooserLanguage;

	public ChooserEnglishLanguageButtonListener (FrameChooserLanguage frameChooserLanguage)
	{
		this.frameChooserLanguage = frameChooserLanguage;
	}
	@Override
	public void actionPerformed (ActionEvent e) 
	{
		// TODO Auto-generated method stub
		frameChooserLanguage.dispose();
		ResourceBundle messages=null;
		Locale currentLocale = new Locale(new String("en"), new String("US"));
		messages = ResourceBundle.getBundle("pl.pw.edu.fizyka.pojava.comptoncrosssections.resources.translation", currentLocale);
		Main.storage = new DataStorage(messages);
		MainWindow projectFrame = new MainWindow(messages);
		SwingUtilities.invokeLater(projectFrame);
	};
	
	
}
